from eval_semantics.models.base import ModelSemanticParticle, ModelSemantics
from eval_semantics.mappings import SemanticParticleMapping, MappingPattern, Empty
from eval_semantics.evaluation.base import (
    general_recursive,
    pattern_evaluate,
    SemanticAtomsAggregation as AtomsAgg,
    ConceptsAggregation as ConceptsAgg,
    SemanticAtomsMetricRaw,
    FinalMeasures,
)

from eval_semantics.semantics.semantics import (
    ConceptSubstitution,
    ConcretizationSubstitution,
    Concept,
    SemanticAtom,
    AtomOperation,
    ParticleAlternative,
)
from functools import reduce
from operator import mul, add
from typing import List
from collections import defaultdict

flatten_list = (
    lambda irregular_list: [
        element for item in irregular_list for element in flatten_list(item)
    ]
    if type(irregular_list) is list
    else [irregular_list]
)

is_activated = lambda a: isinstance(a, tuple) and a[1] == AtomOperation.Activation

is_deactivated = lambda a: isinstance(a, tuple) and a[1] == AtomOperation.Deactivation


def activated_or_nonpossibilistic(atoms):
    filtered = []
    for atom in atoms:
        if isinstance(atom, SemanticAtom):
            filtered.append(atom)
        elif is_activated:
            filtered.append(atom[0])
    return filtered


def atoms_remove_operation(atoms):
    filtered = []
    for atom in atoms:
        if isinstance(atom, SemanticAtom):
            filtered.append(atom)
        elif isinstance(atom, tuple):
            filtered.append(atom[0])
    return filtered


def eval_model_particle_mapping(mapping: SemanticParticleMapping):

    pattern = mapping.pattern.copy(deep=True)

    deactivated_dst = []

    if mapping.alternative != None:
        for inner in pattern.particle.particles:
            if isinstance(inner, ParticleAlternative):
                chosen = inner.alternatives[mapping.alternative]
                inner.alternatives.remove(chosen)

                to_delete = flatten_list(
                    [atoms_remove_operation(alt.atoms) for alt in inner.alternatives]
                )
                retain = activated_or_nonpossibilistic(chosen.atoms)

                to_delete = list(filter(lambda el: el not in retain, to_delete))

                deactivated_dst.extend(to_delete)
                pattern.particle.particles.clear()
                pattern.particle.particles.append(chosen)
                pattern.particle.concepts.clear()
                pattern.particle.concepts = chosen.concepts[:]

    for inner in pattern.particle.particles:
        if mapping.alternative == None and isinstance(inner, ParticleAlternative):
            raise Exception(
                f"inner particle of '{pattern.particle.name}' is alternative, but variant not set"
            )
        for atom in inner.atoms:
            if is_deactivated(atom):
                deactivated_dst.append(atom[0])

    deactivated_src = [
        el[0] for el in list(filter(is_deactivated, mapping.particle.atoms))
    ]

    to_delete = []

    concepts = []
    for concept in pattern.particle.concepts:
        if isinstance(concept, ConceptSubstitution):
            concepts.append(concept.substituted)
            concepts.append(concept.substituting)
        else:
            concepts.append(concept)

    for rule in pattern.rules:

        out_concept_rule = all([isinstance(el, Concept) for el in rule.output])
        out_atom_rule = all([isinstance(el, SemanticAtom) for el in rule.output])

        for deatom in deactivated_dst:
            if deatom in rule.output:
                to_delete.append(rule.symbol)

        for deatom in deactivated_src:
            if deatom in rule.input:
                # if pattern.name == "role_orm":
                #     print(rule.input, "deactivated src")
                to_delete.append(rule.symbol)

        if out_concept_rule:

            def isin(el, l):

                if isinstance(el, tuple):
                    el = el[0]

                inside = el in l
                if inside:
                    return True

                for concept in l:
                    if isinstance(concept, tuple):
                        concept = concept[0]
                    if concept.general and el in concept.general:
                        return True
                return False

            is_not_subset = not all([isin(c, concepts) for c in rule.output])

            if is_not_subset:
                to_delete.append(rule.symbol)

        if out_atom_rule:
            out_concepts = flatten_list([atom.concepts for atom in rule.output])

            def isin(el, l):

                if isinstance(el, tuple):
                    el = el[0]

                inside = el in l
                if inside:
                    return True

                for concept in l:
                    if isinstance(concept, tuple):
                        concept = concept[0]
                    if concept.general and el in concept.general:
                        return True
                return False

            is_not_subset = not all([isin(c, concepts) for c in out_concepts])

            if is_not_subset:
                # if pattern.name == "role_orm":
                #     print(out_concepts, concepts)
                #     print(rule.symbol, rule.input, "is not subset (out atom rule)")
                to_delete.append(rule.symbol)

    pattern.rules = list(filter(lambda r: r.symbol not in to_delete, pattern.rules))

    atoms, concepts = pattern_evaluate(pattern)

    # if pattern.name == "role_uml":
    #     print(pattern.name)
    #     print(20 * "--")
    #     print(atoms)
    #     print(20 * "--")
    #     print(concepts)
    #     print(20 * "--")
    #     for r in pattern.rules:
    #         print(r)
    #     print(20 * "--")
    #     print(AtomsAgg.aggregate(atoms), "\t\t", ConceptsAgg.aggregate(concepts))
    #     print(20 * "--")

    return AtomsAgg.aggregate(atoms), ConceptsAgg.aggregate(concepts)


def eval_model(model: ModelSemantics, metamodel_name: str):

    mpset = model.mappings[metamodel_name]

    eval_sa = []
    eval_c = []

    for mp in mpset:
        sa, c = eval_model_particle_mapping(mp)
        eval_sa.append(sa)
        eval_c.append(c)

    sum_sa = reduce(add, eval_sa)
    sum_c = reduce(add, eval_c)

    atom_measures = (sum_sa.TSA(), sum_sa.ASA(), sum_sa.FSA(), sum_sa.LSA())
    concept_measures = (sum_c.TC(), sum_c.AC(), sum_c.FC(), sum_c.LC())
    final_measures = FinalMeasures.compute(sum_sa, sum_c)

    return atom_measures, concept_measures, final_measures

    # print(mpeval)
