from eval_semantics.semantics.semantics import (
    SemanticParticle,
    SemanticAtom,
    ConceptSubstitution,
)

import json


class ModelSemanticParticle(SemanticParticle):
    base: SemanticParticle

    @classmethod
    def serialize_name(cls, particle):
        return particle.name

    @classmethod
    def serialize(cls, particle):
        def format_atom(atom):
            if isinstance(atom, SemanticAtom):
                return str(atom)
            if isinstance(atom, tuple):
                return (str(atom[0]), atom[1])

        def format_concept(concept):
            if isinstance(concept, ConceptSubstitution):
                return ConceptSubstitution.serialize(concept)
            return concept

        particle.atoms = [format_atom(atom) for atom in particle.atoms]
        particle.concepts = [format_concept(concept) for concept in particle.concepts]
        particle.base = particle.base.name

        return json.loads(particle.json(exclude_unset=True, exclude={"model"}))
