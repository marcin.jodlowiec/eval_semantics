from eval_semantics.semantics.semantics import (
    CategorizationSubstitution,
    Concept,
    SemanticParticle,
    AtomOperation,
    ParticleAlternative,
    ConcretizationSubstitution,
)

from eval_semantics import pyclom
from eval_semantics.metamodels.eer import eer
from eval_semantics.metamodels.aom import aom

# Translational particle
from eval_semantics.mappings import (
    MappingPatternSet,
    Mapping,
    MappingPattern,
    EmptySemanticAtom,
    EmptyConcept,
)

coll_eer = MappingPattern(
    name="coll_eer",
    rules=[
        Mapping(
            symbol="R1",
            input=[aom.concept("Coll")],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R1.1", input=[aom.semantic_atom("1")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.2", input=[aom.semantic_atom("2")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.3", input=EmptySemanticAtom(), output=[eer.semantic_atom("1")]
        ),
    ],
    particle=SemanticParticle(
        name="coll",
        model="eer_aom",
        particles=[
            eer.particle("entity set").with_atom_operation(
                "1", AtomOperation.Deactivation
            )
        ],
        concepts=[eer.concept("EntitySet")],
    ),
)

attr_eer = MappingPattern(
    name="attr_eer",
    rules=[
        Mapping(
            symbol="R1",
            input=[aom.concept("Attr")],
            output=[eer.concept("Attribute")],
        ),
        Mapping(
            symbol="R1.1",
            input=[aom.semantic_atom("3")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.2",
            input=[aom.semantic_atom("4")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.3",
            input=EmptySemanticAtom(),
            output=[eer.semantic_atom("4")],
        ),
        Mapping(
            symbol="R1.4",
            input=EmptySemanticAtom(),
            output=[eer.semantic_atom("5")],
        ),
        Mapping(
            symbol="R1.4",
            input=EmptySemanticAtom(),
            output=[eer.semantic_atom("7")],
        ),
        Mapping(
            symbol="R1.4",
            input=EmptySemanticAtom(),
            output=[eer.semantic_atom("11")],
        ),
        Mapping(
            symbol="R2",
            input=[aom.concept("Coll")],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R2.1",
            input=[aom.semantic_atom("17")],
            output=[eer.semantic_atom("9")],
        ),
        Mapping(
            symbol="R3",
            input=[pyclom.math["positive integer"]],
            output=[pyclom.math["positive integer"], pyclom.math["integer"]],
        ),
        Mapping(
            symbol="R3.1",
            input=[aom.semantic_atom("18")],
            output=[
                eer.semantic_atom("12"),
                eer.semantic_atom("13"),
                eer.semantic_atom("3"),
            ],
        ),
        Mapping(
            symbol="R4",
            input=[aom.concept("DataType")],
            output=[eer.concept("ValueSet")],
        ),
        Mapping(
            symbol="R4.1",
            input=[aom.semantic_atom("19")],
            output=[eer.semantic_atom("10")],
        ),
        Mapping(
            symbol="R4.2",
            input=[aom.semantic_atom("19")],
            output=[eer.semantic_atom("6"), eer.semantic_atom("14")],
        ),
        Mapping(
            symbol="R4",
            input=[aom.concept("Value"), aom.semantic_atom("20")],
            output=[EmptyConcept(), EmptySemanticAtom()],
        ),
    ],
    particle=SemanticParticle(
        name="attr",
        model="eer_aom",
        atoms=[],
        particles=[
            eer.particle("entity set attribute")
            .with_atom_operation("4", AtomOperation.Deactivation)
            .with_atom_operation("7", AtomOperation.Deactivation)
            .with_atom_operation("5", AtomOperation.Deactivation)
            .with_atom_operation("11", AtomOperation.Deactivation),
            eer.particle("complex attribute").with_possibilistic(),
        ],
        concepts=[
            eer.concept("Attribute"),
            eer.concept("EntitySet"),
            eer.concept("ValueSet"),
            pyclom.math["positive integer"],
            pyclom.math["integer"],
            eer.particle("complex attribute").concept("superattribute"),
            eer.particle("complex attribute").concept("subattribute"),
        ],
    ),
)

assoc_eer = MappingPattern(
    name="assoc_eer",
    rules=[
        Mapping(
            symbol="R1",
            input=[aom.concept("Assoc")],
            output=[eer.concept("RelationshipSet")],
        ),
        Mapping(
            symbol="R1.1",
            input=[aom.semantic_atom("1")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.2",
            input=[aom.semantic_atom("2")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.3",
            input=EmptySemanticAtom(),
            output=[eer.semantic_atom("2")],
        ),
        Mapping(
            symbol="R2",
            input=[aom.concept("Assoc")],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R2.1",
            input=EmptySemanticAtom(),
            output=[eer.semantic_atom("1")],
        ),
    ],
    particle=SemanticParticle(
        name="assoc",
        model="eer_aom",
        atoms=[],
        particles=[
            ParticleAlternative(
                alternatives=[
                    eer.particle("entity set").with_atom_operation(
                        "1", AtomOperation.Deactivation
                    ),
                    eer.particle("relationship set").with_atom_operation(
                        "2", AtomOperation.Deactivation
                    ),
                ]
            )
        ],
        concepts=[
            eer.concept("EntitySet"),
            eer.concept("RelationshipSet"),
        ],
    ),
)

role_eer = MappingPattern(
    name="role_eer",
    rules=[
        Mapping(
            symbol="R1",
            input=[aom.concept("Role")],
            output=[eer.concept("Role")],
        ),
        Mapping(
            symbol="R1.1",
            input=[aom.semantic_atom("5")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.2",
            input=[aom.semantic_atom("6")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.3",
            input=[aom.semantic_atom("7")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.4",
            input=[aom.semantic_atom("8")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.5",
            input=[aom.semantic_atom("9")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.6",
            input=[aom.semantic_atom("10")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.7",
            input=[aom.semantic_atom("11")],
            output=[eer.semantic_atom("8")],
        ),
        Mapping(
            symbol="R1.8",
            input=[aom.semantic_atom("12")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R2",
            input=[aom.concept("Role")],
            output=[eer.concept("Role"), eer.concept("RelationshipSet")],
        ),
        Mapping(
            symbol="R3",
            input=[aom.concept("Assoc")],
            output=[eer.concept("EntitySet"), eer.concept("RelationshipSet")],
        ),
        Mapping(
            symbol="R3.1",
            input=[aom.semantic_atom("21")],
            output=[eer.semantic_atom("15")],
        ),
        Mapping(
            symbol="R3.2",
            input=[aom.semantic_atom("21")],
            output=[eer.semantic_atom("18")],
        ),
        Mapping(
            symbol="R4",
            input=[aom.particle("role").concept("role end")],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R4.1",
            input=[aom.semantic_atom("23")],
            output=[eer.semantic_atom("18")],
        ),
        Mapping(
            symbol="R4.2",
            input=[aom.particle("role").concept("role end")],
            output=[eer.semantic_atom("23")],
        ),
        Mapping(
            symbol="R5",
            input=[aom.particle("role").concept("mult supreme in owner")],
            output=[eer.concept("CardinalityKind")],
        ),
        Mapping(
            symbol="R5.1",
            input=[aom.semantic_atom("25")],
            output=[eer.semantic_atom("17")],
        ),
        Mapping(
            symbol="R6",
            input=[aom.particle("role").concept("mult inf in owner")],
            output=[eer.concept("ParticipationKind")],
        ),
        Mapping(
            symbol="R6.1",
            input=[aom.semantic_atom("26")],
            output=[eer.semantic_atom("16")],
        ),
        Mapping(
            symbol="R7",
            input=[
                aom.semantic_atom("27"),
                aom.particle("role").concept("mult supreme in dest"),
            ],
            output=[EmptySemanticAtom(), EmptyConcept()],
        ),
        Mapping(
            symbol="R8",
            input=[
                aom.semantic_atom("28"),
                aom.particle("role").concept("mult inf in dest"),
            ],
            output=[EmptySemanticAtom(), EmptyConcept()],
        ),
        Mapping(
            symbol="R9",
            input=[pyclom.math["positive integer"], aom.semantic_atom("24")],
            output=[EmptySemanticAtom(), EmptyConcept()],
        ),
    ],
    particle=SemanticParticle(
        name="role",
        model="eer_aom",
        particles=[
            eer.particle("relationship role"),
        ],
        concepts=[
            eer.concept("Role"),
            eer.concept("RelationshipSet"),
            eer.concept("EntitySet"),
            eer.concept("CardinalityKind"),
            eer.concept("ParticipationKind"),
        ],
    ),
)

assocdescr_eer = MappingPattern(
    name="assocdescr_eer",
    rules=[
        Mapping(
            symbol="R1",
            input=[
                aom.concept("Assoc"),
            ],
            output=[
                eer.concept("EntitySet"),
            ],
        ),
        Mapping(
            symbol="R1a",
            input=[
                aom.concept("Attr"),
            ],
            output=[
                eer.concept("Attribute"),
            ],
        ),
        Mapping(
            symbol="R1b",
            input=[
                aom.concept("Coll"),
            ],
            output=[
                eer.concept("EntitySet"),
            ],
        ),
        Mapping(
            symbol="R1c",
            input=[
                aom.semantic_atom("22"),
            ],
            output=[
                eer.semantic_atom("9"),
            ],
            force_alternation=True,
        ),
        Mapping(
            symbol="R2",
            input=[
                aom.concept("Assoc"),
            ],
            output=[
                eer.concept("RelationshipSet"),
            ],
        ),
        Mapping(
            symbol="R2a",
            input=[
                aom.concept("Coll"),
            ],
            output=[
                eer.concept("EntitySet"),
            ],
        ),
        Mapping(
            symbol="R2b",
            input=[
                aom.concept("Attr"),
            ],
            output=[
                eer.concept("Attribute"),
            ],
        ),
    ],
    particle=SemanticParticle(
        name="assocdescr",
        model="eer_aom",
        particles=[
            ParticleAlternative(
                alternatives=[
                    eer.particle("relationship set attribute"),
                    eer.particle("entity set attribute"),
                ]
            )
        ],
        concepts=[
            eer.concept("EntitySet"),
            eer.concept("RelationshipSet"),
            eer.concept("Attribute"),
            eer.concept("ValueSet"),
        ],
    ),
)

roledescr_eer = MappingPattern(
    name="roledescr_eer",
    rules=[
        Mapping(
            symbol="IR1",
            input=[aom.semantic_atom("29")],
            output=[eer.semantic_atom("15")],
            force_alternation=True,
        ),
        Mapping(
            symbol="IR2",
            input=[aom.concept("Role")],
            output=EmptyConcept(),
        ),
        Mapping(symbol="IR2", input=EmptyConcept(), output=[eer.concept("Role")]),
        Mapping(
            symbol="IR3",
            input=[aom.concept("Coll")],  # describing coll
            output=[eer.concept("EntitySet")],  # set
        ),
        Mapping(
            symbol="IR4", input=EmptyConcept(), output=[eer.concept("RelationshipSet")]
        ),
    ],
    particle=SemanticParticle(
        name="roledescr",
        model="eer_aom",
        particles=[
            ParticleAlternative(
                alternatives=[
                    eer.particle("relationship set"),
                    eer.particle("relationship role"),
                ]
            )
        ],
        concepts=[
            eer.concept("Role"),
            eer.concept("RelationshipSet"),
            eer.concept("EntitySet"),
            ConcretizationSubstitution(
                substituting=eer.concept("one"),
                substituted=eer.concept("CardinalityKind"),
            ),
            ConcretizationSubstitution(
                substituting=eer.concept("partial"),
                substituted=eer.concept("ParticipationKind"),
            ),
        ],
    ),
)

inheritance_eer = MappingPattern(
    name="inheritance_eer",
    rules=[
        Mapping(
            symbol="R1",
            input=[aom.concept("Inheritance")],
            output=[eer.concept("Generalization/Specialization")],
        ),
        Mapping(
            symbol="R1b",
            input=[aom.concept("Coll")],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R1.1",
            input=[aom.semantic_atom("13")],
            output=[
                eer.semantic_atom("19"),
                eer.semantic_atom("20"),
            ],
            force_translation=True,
        ),
        Mapping(
            symbol="R1.2", input=[aom.semantic_atom("14")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.2", input=[aom.concept("Inheritability")], output=EmptyConcept()
        ),
        Mapping(
            symbol="R1.3", input=[aom.semantic_atom("15")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.3", input=[aom.concept("Inheritability")], output=EmptyConcept()
        ),
        Mapping(
            symbol="R1.4", input=[aom.semantic_atom("16")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.4", input=[aom.concept("Virtuality")], output=EmptyConcept()
        ),
        Mapping(
            symbol="R1.5", input=EmptySemanticAtom(), output=[eer.semantic_atom("21")]
        ),
        Mapping(
            symbol="R1.5",
            input=EmptyConcept(),
            output=[eer.concept("ParticipationKind")],
        ),
        Mapping(
            symbol="R1.6", input=EmptySemanticAtom(), output=[eer.semantic_atom("22")]
        ),
        Mapping(
            symbol="R1.6",
            input=EmptyConcept(),
            output=[eer.concept("DisjointnessKind")],
        ),
        Mapping(symbol="R2", input=[aom.concept("Inheritance")], output=EmptyConcept()),
        Mapping(
            symbol="R2.1-2",
            input=[aom.concept("Assoc")],
            output=[eer.concept("RelationshipSet")],
            force_alternation=True,
        ),
        Mapping(
            symbol="R2.1-2",
            input=[aom.concept("Role")],
            output=[eer.concept("Role")],
            force_alternation=True,
        ),
        Mapping(
            symbol="R2.3",
            input=[aom.semantic_atom("21")],
            output=[eer.semantic_atom("15")],
            force_alternation=True,
        ),
    ],
    particle=SemanticParticle(
        name="inheritance",
        model="eer_aom",
        particles=[
            ParticleAlternative(
                alternatives=[
                    eer.particle("generalization-specialization"),
                    eer.particle("relationship set"),
                ]
            )
        ],
        concepts=[
            eer.concept("Generalization/Specialization"),
            eer.particle("generalization-specialization").concept("generalization"),
            eer.particle("generalization-specialization").concept("specialization"),
            eer.concept("DisjointnessKind"),
            eer.concept("ParticipationKind"),
        ],
    ),
)

bact_eer = MappingPattern(
    name="bact_eer",
    rules=[
        Mapping(
            symbol="R1",
            input=[
                aom.particle("bact").concept("data aspect"),
                aom.particle("bact").concept("relationship aspect"),
                aom.particle("bact").concept("unbreakable connection"),
                pyclom.math["number"](1),
            ],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R1-a",
            input=[aom.semantic_atom("21")],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R1-b",
            input=[aom.semantic_atom("23")],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R1.1", input=EmptySemanticAtom(), output=[eer.semantic_atom("1")]
        ),
        Mapping(
            symbol="R1.2",
            input=[
                aom.concept("Role"),
            ],
            output=[eer.concept("RelationshipSet")],
        ),
        Mapping(
            symbol="R1.3",
            input=[aom.semantic_atom("11")],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R1.4",
            input=[aom.semantic_atom("12")],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R1.5",
            input=[aom.semantic_atom("25")],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R1.6",
            input=[aom.semantic_atom("26")],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R1.7",
            input=[aom.semantic_atom("27")],
            output=[eer.concept("EntitySet")],
        ),
        Mapping(
            symbol="R1.8",
            input=[aom.semantic_atom("28")],
            output=[eer.concept("EntitySet")],
        ),
    ],
    particle=SemanticParticle(
        name="bact",
        model="eer_aom",
        particles=[
            eer.particle("entity set").with_atom_operation(
                "1", AtomOperation.Deactivation
            )
        ],
        concepts=[eer.concept("EntitySet")],
    ),
)

aom2eer = MappingPatternSet(
    source="aom",
    target="eer",
    patterns={
        coll_eer.name: coll_eer,
        attr_eer.name: attr_eer,
        assoc_eer.name: assoc_eer,
        role_eer.name: role_eer,
        assocdescr_eer.name: assocdescr_eer,
        roledescr_eer.name: roledescr_eer,
        inheritance_eer.name: inheritance_eer,
        bact_eer.name: bact_eer,
    },
)
