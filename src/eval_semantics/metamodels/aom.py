from eval_semantics.semantics.semantics import (
    Concept,
    SemanticAtom,
    SemanticParticle,
    CategorizationSubstitution,
    ConcretizationSubstitution,
    AtomOperation,
)
from eval_semantics import pyclom
from eval_semantics.pyclom import clom
from eval_semantics.metamodels.base import repository, MetamodelSemantics


aom = MetamodelSemantics(name="aom")

aom.define_concept("Db", [clom["model"]])
aom.define_concept("BaseNode", [clom["classifier"]])
aom.define_concept("Coll", [aom.concept("BaseNode"), clom["entity type"]])
aom.define_concept("Assoc", [aom.concept("BaseNode"), clom["association"]])
aom.define_concept("Attr", [clom["attribute"]])
aom.define_concept("Role", [clom["role"], clom["association"]])
aom.define_concept("Inheritance", [clom["categorization"]])
aom.define_concept("Value", [clom["atomic entity"]])

aom.define_concept("Composition")
aom.define_concept("inOwner", [aom.concept("Composition")])
aom.define_concept("inDest", [aom.concept("Composition")])
aom.define_concept("inBoth", [aom.concept("Composition")])
aom.define_concept("none", [aom.concept("Composition")])
aom.define_concept("DataType", [clom["domain"]])

for concept in [
    "int8",
    "int16",
    "int32",
    "int64",
    "float",
    "double",
    "bool",
    "ascii",
    "unicode",
    "date",
    "time",
    "datetime",
    "object",
    "reference",
    "byte",
    "ascii-string",
    "unicode-string",
    "byte-array",
]:
    aom.define_concept(concept, [aom.concept("DataType")])

aom.define_concept("Direction")
aom.define_concept("toOwner", [aom.concept("Direction")])
aom.define_concept("toDest", [aom.concept("Direction")])
aom.define_concept("biDir", [aom.concept("Direction")])

aom.define_concept("Inheritability")
aom.define_concept("disable", [aom.concept("Inheritability")])
aom.define_concept("nochange", [aom.concept("Inheritability")])
aom.define_concept("none", [aom.concept("Inheritability")])

aom.define_concept("Navigability")
aom.define_concept("uniNav", [aom.concept("Navigability")])
aom.define_concept("biNav", [aom.concept("Navigability")])

aom.define_concept("Virtuality")
aom.define_concept("natural", [aom.concept("Virtuality")])
aom.define_concept("virtual", [aom.concept("Virtuality")])
aom.define_concept("real", [aom.concept("Virtuality")])

aom.define_semantic_atom(
    id="1",
    is_possibilistic=True,
    verbalization="{0} can be abstract",
    concepts=[aom.concept("BaseNode")],
)

aom.define_semantic_atom(
    id="2",
    is_possibilistic=True,
    verbalization="{0} can be uninavigable",
    concepts=[aom.concept("BaseNode")],
)

aom.define_semantic_atom(
    id="3",
    is_possibilistic=True,
    verbalization="{0} can be inheritable",
    concepts=[aom.concept("Attr")],
)

aom.define_semantic_atom(
    id="4",
    is_possibilistic=True,
    verbalization="{0} can be virtual",
    concepts=[aom.concept("Attr")],
)

aom.define_semantic_atom(
    id="5",
    is_possibilistic=True,
    verbalization="{0} can be virtual",
    concepts=[aom.concept("Role")],
)

aom.define_semantic_atom(
    id="6",
    is_possibilistic=True,
    verbalization="{0} can be inheritable",
    concepts=[aom.concept("Role")],
)

aom.define_semantic_atom(
    id="7",
    is_possibilistic=True,
    verbalization="{0} can be uninavigable",
    concepts=[aom.concept("Role")],
)

aom.define_semantic_atom(
    id="8",
    is_possibilistic=True,
    verbalization="{0} can be directed to owner",
    concepts=[aom.concept("Role")],
)

aom.define_semantic_atom(
    id="9",
    is_possibilistic=True,
    verbalization="{0} can be directed to dest",
    concepts=[aom.concept("Role")],
)

aom.define_semantic_atom(
    id="10",
    is_possibilistic=True,
    verbalization="{0} can be bidirectional",
    concepts=[aom.concept("Role")],
)

aom.define_semantic_atom(
    id="11",
    is_possibilistic=True,
    verbalization="{0} can have composition in owner",
    concepts=[aom.concept("Role")],
)

aom.define_semantic_atom(
    id="12",
    is_possibilistic=True,
    verbalization="{0} can have composition in dest",
    concepts=[aom.concept("Role")],
)

aom.define_semantic_atom(
    id="13",
    is_possibilistic=True,
    verbalization="{0} can inherit from {1}",
    concepts=[
        (aom.concept("BaseNode"), "BaseNode_1"),
        (aom.concept("BaseNode"), "BaseNode_2"),
    ],
)

aom.define_semantic_atom(
    id="14",
    is_possibilistic=False,
    verbalization="{0} is of type {1} in aspect of member inheritance",
    concepts=[
        aom.concept("Inheritance"),
        aom.concept("Inheritability"),
    ],
)

aom.define_semantic_atom(
    id="15",
    is_possibilistic=False,
    verbalization="{0} is of type {1} in aspect of right to fulfill roles inheritance",
    concepts=[
        aom.concept("Inheritance"),
        aom.concept("Inheritability"),
    ],
)

aom.define_semantic_atom(
    id="16",
    is_possibilistic=False,
    verbalization="{0} is of type {1} in terms of virtuality",
    concepts=[
        aom.concept("Inheritance"),
        aom.concept("Virtuality"),
    ],
)

aom.define_semantic_atom(
    id="17",
    is_possibilistic=True,
    verbalization="{0} can have {1}",
    concepts=[
        aom.concept("Coll"),
        aom.concept("Attr"),
    ],
)

aom.define_semantic_atom(
    id="18",
    is_possibilistic=False,
    verbalization="{0} has cardinality of {1}",
    concepts=[
        aom.concept("Attr"),
        pyclom.math["positive integer"],
    ],
)

aom.define_semantic_atom(
    id="19",
    is_possibilistic=False,
    verbalization="{0} is of type {1}",
    concepts=[
        aom.concept("Attr"),
        aom.concept("DataType"),
    ],
)

aom.define_semantic_atom(
    id="20",
    is_possibilistic=False,
    verbalization="{0} has default value {1}",
    concepts=[
        aom.concept("Attr"),
        aom.concept("Value"),
    ],
)

aom.define_semantic_atom(
    id="21",
    is_possibilistic=True,
    verbalization="{0} can have {1}",
    concepts=[
        aom.concept("Assoc"),
        aom.concept("Role"),
    ],
)

aom.define_semantic_atom(
    id="22",
    is_possibilistic=True,
    verbalization="{0} can be described by {1}",
    concepts=[
        aom.concept("Assoc"),
        aom.concept("Coll"),
    ],
)

aom.define_semantic_atom(
    id="23",
    is_possibilistic=True,
    verbalization="{0} can participate in {1}",
    concepts=[
        aom.concept("BaseNode"),
        aom.concept("Role"),
    ],
)

aom.define_semantic_atom(
    id="24",
    is_possibilistic=False,
    verbalization="{0} can have uniqueness equal to {1}",
    concepts=[
        aom.concept("Role"),
        pyclom.math["positive integer"],
    ],
)

aom.define_semantic_atom(
    id="25",
    is_possibilistic=False,
    verbalization="{0} has upper bound equal to {1} in owner",
    concepts=[
        aom.concept("Role"),
        pyclom.math["unlimited integer"],
    ],
)

aom.define_semantic_atom(
    id="26",
    is_possibilistic=False,
    verbalization="{0} has lower bound equal to {1} in owner",
    concepts=[
        aom.concept("Role"),
        pyclom.math["integer"],
    ],
)

aom.define_semantic_atom(
    id="27",
    is_possibilistic=False,
    verbalization="{0} has upper bound equal to {1} in dest",
    concepts=[
        aom.concept("Role"),
        pyclom.math["unlimited integer"],
    ],
)

aom.define_semantic_atom(
    id="28",
    is_possibilistic=False,
    verbalization="{0} has lower bound equal to {1} in dest",
    concepts=[
        aom.concept("Role"),
        pyclom.math["integer"],
    ],
)

aom.define_semantic_atom(
    id="29",
    is_possibilistic=True,
    verbalization="{0} can be described by {1}",
    concepts=[
        aom.concept("Role"),
        aom.concept("Coll"),
    ],
)

aom.define_semantic_particle(
    name="collection",
    atoms=[aom.semantic_atom("1"), aom.semantic_atom("2")],
    concepts=[
        CategorizationSubstitution(
            substituting=aom.concept("Coll"), substituted=aom.concept("BaseNode")
        )
    ],
)

aom.define_semantic_particle(
    name="association",
    atoms=[aom.semantic_atom("1"), aom.semantic_atom("2")],
    concepts=[
        CategorizationSubstitution(
            substituting=aom.concept("Assoc"), substituted=aom.concept("BaseNode")
        )
    ],
)

aom.define_semantic_particle(
    name="attribute",
    atoms=[
        aom.semantic_atom("3"),
        aom.semantic_atom("4"),
        (aom.semantic_atom("17"), AtomOperation.Activation),
        aom.semantic_atom("18"),
        aom.semantic_atom("19"),
        aom.semantic_atom("20"),
    ],
    concepts=[
        aom.concept("Attr"),
        aom.concept("Coll"),
        aom.concept("Value"),
        aom.concept("DataType"),
        ConcretizationSubstitution(
            substituting=Concept(name="attribute multiplicity"),
            substituted=pyclom.math["positive integer"],
            context=[aom.semantic_atom("18")],
        ),
    ],
)


aom.define_semantic_particle(
    name="role",
    atoms=[
        aom.semantic_atom("5"),
        aom.semantic_atom("6"),
        aom.semantic_atom("7"),
        aom.semantic_atom("8"),
        aom.semantic_atom("9"),
        aom.semantic_atom("10"),
        aom.semantic_atom("11"),
        aom.semantic_atom("12"),
        aom.semantic_atom("25"),
        aom.semantic_atom("26"),
        aom.semantic_atom("27"),
        aom.semantic_atom("28"),
        aom.semantic_atom("24"),
        (aom.semantic_atom("21"), AtomOperation.Activation),
        (aom.semantic_atom("23"), AtomOperation.Activation),
    ],
    concepts=[
        aom.concept("Role"),
        aom.concept("Assoc"),
        ConcretizationSubstitution(
            substituting=Concept(name="role end"),
            substituted=aom.concept("BaseNode"),
            context=[aom.semantic_atom("23")],
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="mult supreme in owner"),
            substituted=pyclom.math["unlimited integer"],
            context=[aom.semantic_atom("25")],
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="mult inf in owner"),
            substituted=pyclom.math["integer"],
            context=[aom.semantic_atom("26")],
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="mult supreme in dest"),
            substituted=pyclom.math["unlimited integer"],
            context=[aom.semantic_atom("27")],
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="mult inf in dest"),
            substituted=pyclom.math["integer"],
            context=[aom.semantic_atom("28")],
        ),
    ],
)

aom.define_semantic_particle(
    name="association description",
    atoms=[(aom.semantic_atom("22"), AtomOperation.Activation)],
    concepts=[aom.concept("Assoc"), aom.concept("Coll")],
)

aom.define_semantic_particle(
    name="role description",
    atoms=[(aom.semantic_atom("29"), AtomOperation.Activation)],
    concepts=[aom.concept("Role"), aom.concept("Coll")],
)

aom.define_semantic_particle(
    name="inheritance",
    atoms=[
        (aom.semantic_atom("13"), AtomOperation.Activation),
        aom.semantic_atom("14"),
        aom.semantic_atom("15"),
        aom.semantic_atom("16"),
    ],
    concepts=[
        (aom.concept("BaseNode"), "BaseNode_1"),
        (aom.concept("BaseNode"), "BaseNode_2"),
        aom.concept("Inheritance"),
        aom.concept("Virtuality"),
        ConcretizationSubstitution(
            substituting=Concept(name="member inheritance aspect", individual=True),
            substituted=aom.concept("Inheritability"),
            context=[aom.semantic_atom("14")],
        ),
        ConcretizationSubstitution(
            substituting=Concept(
                name="fulfill roles inheritance aspect", individual=True
            ),
            substituted=aom.concept("Inheritability"),
            context=[aom.semantic_atom("15")],
        ),
    ],
)

aom.define_semantic_particle(
    name="bact",
    atoms=[
        aom.semantic_atom("1"),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
        (aom.semantic_atom("11"), AtomOperation.Activation),
        (aom.semantic_atom("12"), AtomOperation.Activation),
        aom.semantic_atom("25"),
        aom.semantic_atom("26"),
        aom.semantic_atom("27"),
        aom.semantic_atom("28"),
        (aom.semantic_atom("21"), AtomOperation.Activation),
        (aom.semantic_atom("23"), AtomOperation.Activation),
    ],
    concepts=[
        ConcretizationSubstitution(
            substituting=Concept(name="data aspect", individual=True),
            substituted=aom.concept("Coll"),
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="relationship aspect", individual=True),
            substituted=aom.concept("Assoc"),
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="unbreakable connection", individual=True),
            substituted=aom.concept("Role"),
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="data aspect", individual=True),
            substituted=Concept(name="role end", individual=True),
            context=[aom.semantic_atom("23")],
        ),
        ConcretizationSubstitution(
            substituting=pyclom.math["number"](1),
            substituted=pyclom.math["unlimited integer"],
        ),
    ],
)
