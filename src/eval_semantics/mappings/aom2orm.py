from eval_semantics.semantics.semantics import (
    CategorizationSubstitution,
    Concept,
    SemanticParticle,
    AtomOperation,
    ParticleAlternative,
    ConcretizationSubstitution,
)

from eval_semantics import pyclom
from eval_semantics.metamodels.orm import orm
from eval_semantics.metamodels.aom import aom

# Translational particle
from eval_semantics.mappings import (
    MappingPatternSet,
    Mapping,
    MappingPattern,
    EmptySemanticAtom,
    EmptyConcept,
)


coll_orm = MappingPattern(
    name="coll_orm",
    rules=[
        Mapping(
            symbol="R1",
            input=[aom.concept("Coll")],
            output=[orm.concept("EntityType")],
        ),
        Mapping(
            symbol="R1.1", input=[aom.semantic_atom("1")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.2", input=[aom.semantic_atom("2")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.3", input=EmptySemanticAtom(), output=[orm.semantic_atom("2")]
        ),
    ],
    particle=SemanticParticle(
        name="coll",
        model="orm_aom",
        particles=[
            orm.particle("object-entity").with_atom_operation(
                "2", AtomOperation.Activation
            )
        ],
        concepts=[orm.concept("EntityType")],
    ),
)

attr_orm = MappingPattern(
    name="attr_orm",
    rules=[
        Mapping(
            symbol="R1",
            input=[
                aom.concept("Attr"),
            ],
            output=[
                orm.concept("ValueType"),
                orm.concept("Role"),
            ],
        ),
        Mapping(
            symbol="R1.1", input=[aom.semantic_atom("3")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.2", input=[aom.semantic_atom("4")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R2",
            input=[
                aom.concept("Coll"),
            ],
            output=[
                orm.concept("EntityType"),
            ],
        ),
        Mapping(
            symbol="R2.1",
            input=[aom.semantic_atom("17")],
            output=[
                orm.semantic_atom("8"),
                orm.semantic_atom("9"),
            ],
        ),
        Mapping(
            symbol="R2.2",
            input=[EmptySemanticAtom(), EmptyConcept()],
            output=[orm.semantic_atom("18"), pyclom.math["integer"]],
        ),
        Mapping(
            symbol="R3",
            input=[aom.semantic_atom("18"), pyclom.math["positive integer"]],
            output=[orm.semantic_atom("18"), pyclom.math["integer"]],
        ),
        Mapping(
            symbol="R4",
            input=[aom.concept("DataType"), aom.semantic_atom("19")],
            output=[EmptyConcept(), EmptySemanticAtom()],
        ),
        Mapping(
            symbol="R5",
            input=[aom.concept("Value"), aom.semantic_atom("20")],
            output=[EmptyConcept(), EmptySemanticAtom()],
        ),
    ],
    particle=SemanticParticle(
        name="attr",
        model="orm_aom",
        particles=[
            orm.particle("role"),
            orm.particle("role"),
            orm.particle("object-entity"),
            orm.particle("object-value"),
            orm.particle("fact"),
        ],
        concepts=[
            ConcretizationSubstitution(
                substituting=Concept(name="entity", individual=True),
                substituted=orm.concept("EntityType"),
            ),
            ConcretizationSubstitution(
                substituting=Concept(name="attribute", individual=True),
                substituted=orm.concept("ValueType"),
            ),
            ConcretizationSubstitution(
                substituting=Concept(name="entity role", individual=True),
                substituted=orm.concept("Role"),
            ),
            ConcretizationSubstitution(
                substituting=Concept(name="attribute role", individual=True),
                substituted=orm.concept("Role"),
            ),
            orm.concept("FactType"),
        ],
    ),
)

assoc_orm = MappingPattern(
    name="assoc_orm",
    rules=[
        Mapping(
            symbol="R1", input=[aom.concept("Assoc")], output=[orm.concept("FactType")]
        ),
        Mapping(
            symbol="R1.1", input=[aom.semantic_atom("1")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.2", input=[aom.semantic_atom("2")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.3", input=EmptySemanticAtom(), output=[orm.semantic_atom("1")]
        ),
    ],
    particle=SemanticParticle(
        name="assoc",
        model="orm_aom",
        particles=[
            orm.particle("fact"),
            orm.particle("reification").with_possibilistic(),
        ],
        concepts=[orm.concept("FactType"), orm.concept("EntityType")],
    ),
)

role_orm = MappingPattern(
    name="role_orm",
    rules=[
        Mapping(symbol="R1", input=[aom.concept("Role")], output=[orm.concept("Role")]),
        Mapping(
            symbol="R1.1", input=[aom.semantic_atom("5")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.2", input=[aom.semantic_atom("6")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.3", input=[aom.semantic_atom("7")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.4", input=[aom.semantic_atom("8")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.5", input=[aom.semantic_atom("9")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.6", input=[aom.semantic_atom("10")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.7", input=[aom.semantic_atom("11")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.8", input=[aom.semantic_atom("12")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R1.9",
            input=[aom.semantic_atom("24"), pyclom.math["positive integer"]],
            output=[orm.semantic_atom("5")],
            force_alternation=True,
        ),
        Mapping(
            symbol="R2",
            input=[
                aom.particle("role").concept("mult supreme in dest"),
                aom.particle("role").concept("mult inf in dest"),
            ],
            output=[pyclom.math["integer"]],
        ),
        Mapping(
            symbol="R2.1",
            input=[aom.semantic_atom("27"), aom.semantic_atom("28")],
            output=[orm.semantic_atom("18")],
            force_translation=True,
        ),
        Mapping(
            symbol="R2.2",
            input=[aom.semantic_atom("27"), aom.semantic_atom("28")],
            output=[orm.semantic_atom("19")],
            force_translation=True,
        ),
        Mapping(
            symbol="R2.3",
            input=[aom.semantic_atom("27"), aom.semantic_atom("28")],
            output=[orm.semantic_atom("20")],
            force_translation=True,
        ),
        Mapping(
            symbol="R2.4",
            input=[aom.semantic_atom("27"), aom.semantic_atom("28")],
            output=[orm.semantic_atom("21")],
            force_translation=True,
        ),
        Mapping(
            symbol="R3",
            input=[
                aom.particle("role").concept("mult supreme in owner"),
            ],
            output=[EmptyConcept(), EmptySemanticAtom()],
        ),
        Mapping(
            symbol="R4",
            input=[
                aom.particle("role").concept("mult inf in owner"),
                aom.semantic_atom("26"),
            ],
            output=[EmptyConcept(), EmptySemanticAtom()],
        ),
        Mapping(
            symbol="R5",
            input=[aom.particle("role").concept("role end")],
            output=[orm.concept("ObjectType")],
        ),
        Mapping(
            symbol="R5.1",
            input=[aom.semantic_atom("23")],
            output=[orm.semantic_atom("8")],
            force_translation=True,
        ),
        Mapping(
            symbol="R5.2",
            input=[aom.particle("role").concept("role end"), aom.concept("Assoc")],
            output=[orm.semantic_atom("10")],
            force_alternation=True,
        ),
        Mapping(
            symbol="R6", input=[aom.concept("Assoc")], output=[orm.concept("FactType")]
        ),
        Mapping(
            symbol="R6.1",
            input=[aom.semantic_atom("21")],
            output=[orm.semantic_atom("9")],
            force_translation=True,
        ),
    ],
    particle=SemanticParticle(
        name="role",
        model="orm_aom",
        particles=[
            orm.particle("role"),
        ],
        concepts=[
            orm.concept("FactType"),
            orm.concept("EntityType"),
            orm.concept("ObjectType"),
            orm.concept("Role"),
            orm.particle("role").concept("freq_min"),
            orm.particle("role").concept("freq_max"),
        ],
    ),
)

assocdescr_orm = MappingPattern(
    name="assocdescr_orm",
    rules=[
        Mapping(
            symbol="R1", input=[aom.concept("Assoc")], output=[orm.concept("FactType")]
        ),
        Mapping(
            symbol="R2",
            input=[aom.concept("Coll")],
            output=[orm.concept("EntityType")],
        ),
        Mapping(
            symbol="R3",
            input=[aom.semantic_atom("22"), EmptyConcept()],
            output=[
                orm.semantic_atom("8"),
                orm.semantic_atom("9"),
                orm.concept("Role"),
            ],
        ),
        Mapping(
            symbol="R3.1",
            input=[EmptySemanticAtom(), EmptyConcept()],
            output=[orm.semantic_atom("20"), pyclom.math["integer"]],
        ),
    ],
    particle=SemanticParticle(
        name="assocdescr",
        model="orm_aom",
        particles=[
            orm.particle("role").with_atom_operation("5", AtomOperation.Activation)
        ],
        concepts=[
            ConcretizationSubstitution(
                substituting=Concept(name="description", individual=True),
                substituted=orm.concept("Role"),
            ),
            CategorizationSubstitution(
                substituting=orm.concept("EntityType"),
                substituted=orm.concept("ObjectType"),
            ),
            orm.concept("FactType"),
            ConcretizationSubstitution(
                context=[orm.semantic_atom("20")],
                substituting=pyclom.math["number"](1),
                substituted=pyclom.math["integer"],
            ),
        ],
    ),
)


roledescr_orm = MappingPattern(
    name="roledescr_orm",
    rules=[
        Mapping(
            symbol="IR1",
            input=[aom.semantic_atom("29")],
            output=[orm.semantic_atom("9")],
            force_alternation=True,
        ),
        Mapping(
            symbol="IR2",
            input=[aom.concept("Role")],
            output=EmptyConcept(),
        ),
        Mapping(symbol="IR2", input=EmptyConcept(), output=[orm.concept("Role")]),
        Mapping(
            symbol="IR3",
            input=[aom.concept("Coll")],  # describing coll
            output=[orm.concept("EntityType")],  # set
        ),
        Mapping(symbol="IR4", input=EmptyConcept(), output=[orm.concept("FactType")]),
    ],
    particle=SemanticParticle(
        name="roledescr",
        model="orm_aom",
        particles=[
            orm.particle("role"),
        ],
        concepts=[
            orm.concept("Role"),
            orm.concept("FactType"),
            orm.concept("EntityType"),
        ],
    ),
)


inheritance_orm = MappingPattern(
    name="inheritance_orm",
    rules=[
        Mapping(
            symbol="R1",
            input=[aom.concept("Inheritance")],
            output=[orm.concept("Subtyping")],
        ),
        Mapping(
            symbol="R1.1",
            input=[aom.concept("Coll"), aom.semantic_atom("13")],
            output=[
                orm.concept("ObjectType"),
                orm.semantic_atom("11"),
                orm.semantic_atom("12"),
            ],
            force_translation=True,
        ),
        Mapping(
            symbol="R1.2",
            input=[aom.concept("Assoc"), aom.semantic_atom("13")],
            output=[
                orm.concept("ObjectType"),
                orm.semantic_atom("11"),
                orm.semantic_atom("12"),
            ],
            force_translation=True,
        ),
        Mapping(
            symbol="R2",
            input=[aom.concept("Inheritability"), aom.semantic_atom("14")],
            output=[EmptyConcept(), EmptySemanticAtom()],
        ),
        Mapping(
            symbol="R3",
            input=[aom.concept("Inheritability"), aom.semantic_atom("15")],
            output=[EmptyConcept(), EmptySemanticAtom()],
        ),
        Mapping(
            symbol="R4",
            input=[aom.concept("Virtuality"), aom.semantic_atom("16")],
            output=[EmptyConcept(), EmptySemanticAtom()],
        ),
    ],
    particle=SemanticParticle(
        name="inheritance",
        model="orm_aom",
        particles=[
            orm.particle("subtyping"),
            orm.particle("reification").with_possibilistic(),
        ],
        concepts=[
            orm.concept("Subtyping"),
            orm.particle("subtyping").concept("supertype"),
            orm.particle("subtyping").concept("subtype"),
            (orm.concept("FactType"), "FactType_1"),
            (orm.concept("FactType"), "FactType_2"),
        ],
    ),
)


bact_orm = MappingPattern(
    name="bact_orm",
    rules=[
        Mapping(
            symbol="R1",
            input=[
                aom.particle("bact").concept("data aspect"),
                aom.semantic_atom("23"),
            ],
            output=[orm.concept("EntityType"), orm.semantic_atom("8")],
        ),
        Mapping(
            symbol="R2",
            input=[
                aom.particle("bact").concept("relationship aspect"),
                aom.semantic_atom("21"),
            ],
            output=[orm.concept("FactType"), orm.semantic_atom("9")],
        ),
        Mapping(
            symbol="R3",
            input=[
                aom.particle("bact").concept("unbreakable connection"),
                pyclom.math["number"](1),
            ],
            output=[orm.concept("Role")],
        ),
        Mapping(
            symbol="R3.1",
            input=[aom.semantic_atom("11")],
            output=[orm.concept("Role"), EmptySemanticAtom()],
        ),
        Mapping(
            symbol="R3.2",
            input=[aom.semantic_atom("12")],
            output=[orm.concept("Role"), EmptySemanticAtom()],
        ),
        Mapping(
            symbol="R3.3",
            input=[aom.semantic_atom("26")],
            output=[orm.semantic_atom("5")],
        ),
        Mapping(
            symbol="R3.4",
            input=[aom.semantic_atom("25")],
            output=[orm.semantic_atom("4")],
        ),
        Mapping(
            symbol="R3.5",
            input=[
                pyclom.math["number"](1),
                aom.semantic_atom("27"),
                aom.semantic_atom("28"),
            ],
            output=[pyclom.math["number"](1), orm.semantic_atom("18")],
        ),
    ],
    particle=SemanticParticle(
        name="bact",
        model="orm_aom",
        particles=[
            orm.particle("object-entity"),
            orm.particle("role")
            .with_atom_operation("4", AtomOperation.Activation)
            .with_atom_operation("5", AtomOperation.Activation)
            .with_atom_operation("18", AtomOperation.Activation),
            orm.particle("reification"),
        ],
        concepts=[
            orm.concept("EntityType"),
            orm.concept("Role"),
            orm.concept("FactType"),
        ],
    ),
)


aom2orm = MappingPatternSet(
    source="aom",
    target="orm",
    patterns={
        coll_orm.name: coll_orm,
        attr_orm.name: attr_orm,
        assoc_orm.name: assoc_orm,
        role_orm.name: role_orm,
        assocdescr_orm.name: assocdescr_orm,
        roledescr_orm.name: roledescr_orm,
        inheritance_orm.name: inheritance_orm,
        bact_orm.name: bact_orm,
    },
)
