from eval_semantics.semantics.semantics import (
    Concept,
    SemanticAtom,
    SemanticParticle,
    CategorizationSubstitution,
    ConcretizationSubstitution,
    AtomOperation,
)
from eval_semantics import pyclom
from eval_semantics.pyclom import clom
from eval_semantics.metamodels.base import MetamodelSemantics


orm = MetamodelSemantics(name="orm")


orm.define_concept("ObjectType", [clom["entity type"]])
orm.define_concept("EntityType", [orm.concept("ObjectType")])
orm.define_concept("ValueType", [orm.concept("ObjectType"), clom["atomic type"]])
orm.define_concept("Role", [clom["role"], clom["attribute"]])
orm.define_concept("FactType", [clom["association"]])
orm.define_concept("Subtyping", [clom["categorization"]])
orm.define_concept("Value", [clom["atomic entity"]])
orm.define_concept("ValueRange", [])
orm.define_concept("ValueList", [clom["domain"]])
orm.define_concept("RingKind", [])

for ring in [
    "irreflexive",
    "asymmetric",
    "instransitive",
    "antisymmetric",
    "acyclic",
    "asymmetric+intransitive",
    "acyclic+intransitive",
    "symmetric",
    "symmetric+irreflexive",
    "symmetric+intransitive",
    "purly reflexive",
]:
    orm.define_concept(ring, [orm.concept("RingKind")])


orm.define_semantic_atom(
    id="1",
    is_possibilistic=True,
    verbalization="{0} can be derived",
    concepts=[orm.concept("FactType")],
)

orm.define_semantic_atom(
    id="2",
    is_possibilistic=True,
    verbalization="{0} can be independent",
    concepts=[orm.concept("ObjectType")],
)

orm.define_semantic_atom(
    id="3",
    is_possibilistic=True,
    verbalization="{0} can be primitive",
    concepts=[orm.concept("ObjectType")],
)

orm.define_semantic_atom(
    id="4",
    is_possibilistic=True,
    verbalization="{0} can be constrained by mandatory",
    concepts=[orm.concept("Role")],
)

orm.define_semantic_atom(
    id="5",
    is_possibilistic=True,
    verbalization="{0} can be constrained by uniqueness",
    concepts=[orm.concept("Role")],
)

orm.define_semantic_atom(
    id="6",
    is_possibilistic=True,
    verbalization="{0} can be constrained by totality",
    concepts=[orm.concept("Subtyping")],
)

orm.define_semantic_atom(
    id="7",
    is_possibilistic=True,
    verbalization="{0} can be constrained by disjointness",
    concepts=[orm.concept("Subtyping")],
)

orm.define_semantic_atom(
    id="8",
    is_possibilistic=True,
    verbalization="{0} can play {1}",
    concepts=[orm.concept("ObjectType"), orm.concept("Role")],
)

orm.define_semantic_atom(
    id="9",
    is_possibilistic=False,
    verbalization="{0} is in {1}",
    concepts=[orm.concept("Role"), orm.concept("FactType")],
)

orm.define_semantic_atom(
    id="10",
    is_possibilistic=True,
    verbalization="{0} can reify {1}",
    concepts=[orm.concept("EntityType"), orm.concept("FactType")],
)

orm.define_semantic_atom(
    id="11",
    is_possibilistic=False,
    verbalization="{0} has supertype {1}",
    concepts=[orm.concept("Subtyping"), orm.concept("ObjectType")],
)

orm.define_semantic_atom(
    id="12",
    is_possibilistic=False,
    verbalization="{0} has subtype {1}",
    concepts=[orm.concept("Subtyping"), orm.concept("ObjectType")],
)

orm.define_semantic_atom(
    id="13",
    is_possibilistic=True,
    verbalization="{0} can be constrained by {1}",
    concepts=[orm.concept("ValueType"), orm.concept("ValueList")],
)

orm.define_semantic_atom(
    id="14",
    is_possibilistic=True,
    verbalization="{0} can be constrained by {1}",
    concepts=[orm.concept("Role"), orm.concept("ValueList")],
)

orm.define_semantic_atom(
    id="15",
    is_possibilistic=True,
    verbalization="{0} can be subset of {1}",
    concepts=[(orm.concept("Role"), "Role_1"), (orm.concept("ValueList"), "Role_2")],
)

orm.define_semantic_atom(
    id="16",
    is_possibilistic=True,
    verbalization="{0} can be equal to {1}",
    concepts=[(orm.concept("Role"), "Role_1"), (orm.concept("ValueList"), "Role_2")],
)

orm.define_semantic_atom(
    id="17",
    is_possibilistic=True,
    verbalization="{0} and {1} can be mutually exclusive",
    concepts=[(orm.concept("Role"), "Role_1"), (orm.concept("ValueList"), "Role_2")],
)

orm.define_semantic_atom(
    id="18",
    is_possibilistic=True,
    verbalization="{0} can be constrained by frequency equal to {1} ",
    concepts=[orm.concept("Role"), pyclom.math["integer"]],
)

orm.define_semantic_atom(
    id="19",
    is_possibilistic=True,
    verbalization="{0} can be constrained by frequency at least {1} ",
    concepts=[orm.concept("Role"), pyclom.math["integer"]],
)

orm.define_semantic_atom(
    id="20",
    is_possibilistic=True,
    verbalization="{0} can be constrained by frequency at most {1} ",
    concepts=[orm.concept("Role"), pyclom.math["integer"]],
)

orm.define_semantic_atom(
    id="21",
    is_possibilistic=True,
    verbalization="{0} can be constrained by frequency from range {1} - {2}",
    concepts=[
        orm.concept("Role"),
        (pyclom.math["integer"], "int_1"),
        (pyclom.math["integer"], "int_2"),
    ],
)

orm.define_semantic_atom(
    id="22",
    is_possibilistic=True,
    verbalization="{0} and {1} can be in ring constraint {3} ",
    concepts=[
        (orm.concept("Role"), "Role_1"),
        (orm.concept("ValueList"), "Role_2"),
        orm.concept("RingKind"),
    ],
)


orm.define_semantic_particle(
    name="object-entity",
    atoms=[orm.semantic_atom("2"), orm.semantic_atom("3")],
    concepts=[
        CategorizationSubstitution(
            substituting=orm.concept("EntityType"),
            substituted=orm.concept("ObjectType"),
        )
    ],
)

orm.define_semantic_particle(
    name="object-value",
    atoms=[orm.semantic_atom("2"), orm.semantic_atom("13")],
    concepts=[
        CategorizationSubstitution(
            substituting=orm.concept("ValueType"),
            substituted=orm.concept("ObjectType"),
        )
    ],
)

orm.define_semantic_particle(
    name="fact",
    atoms=[
        orm.semantic_atom("1"),
    ],
    concepts=[
        orm.concept("FactType"),
    ],
)

orm.define_semantic_particle(
    name="role",
    atoms=[
        orm.semantic_atom("4"),
        orm.semantic_atom("5"),
        (orm.semantic_atom("8"), AtomOperation.Activation),
        orm.semantic_atom("9"),
        orm.semantic_atom("14"),
        orm.semantic_atom("15"),
        orm.semantic_atom("16"),
        orm.semantic_atom("17"),
        orm.semantic_atom("18"),
        orm.semantic_atom("19"),
        orm.semantic_atom("20"),
        orm.semantic_atom("21"),
        orm.semantic_atom("22"),
    ],
    concepts=[
        orm.concept("Role"),
        orm.concept("FactType"),
        orm.concept("ObjectType"),
        orm.concept("RingKind"),
        ConcretizationSubstitution(
            substituting=Concept(name="freq_min", individual=True),
            substituted=pyclom.math["integer"],
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="freq_max", individual=True),
            substituted=pyclom.math["integer"],
        ),
    ],
)

orm.define_semantic_particle(
    name="subtyping",
    atoms=[
        orm.semantic_atom("11"),
        orm.semantic_atom("12"),
    ],
    concepts=[
        orm.concept("Subtyping"),
        ConcretizationSubstitution(
            context=[orm.semantic_atom("11")],
            substituted=orm.concept("ObjectType"),
            substituting=Concept(name="supertype", individual=True),
        ),
        ConcretizationSubstitution(
            context=[orm.semantic_atom("12")],
            substituted=orm.concept("ObjectType"),
            substituting=Concept(name="subtype", individual=True),
        ),
    ],
)

orm.define_semantic_particle(
    name="subtype set",
    atoms=[
        orm.semantic_atom("6"),
        orm.semantic_atom("7"),
    ],
    concepts=[
        orm.concept("Subtyping"),
    ],
)

orm.define_semantic_particle(
    name="reification",
    atoms=[(orm.semantic_atom("10"), AtomOperation.Activation)],
    concepts=[
        orm.concept("EntityType"),
        orm.concept("FactType"),
    ],
)
