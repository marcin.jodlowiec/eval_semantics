import json
from pydantic import BaseModel as Base, parse_raw_as
from typing import Optional, List
from pathlib import Path


from eval_semantics.mappings import repository as mapping_repository
from eval_semantics.metamodels.base import repository as metamodel_repository


class Config(Base):
    metamodels: List[Path]
    mappings: List[Path]
    dir: Path

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Load metamodels from config
        for filename in self.metamodels:
            metamodel_repository.load_from_json(self.dir / filename)

        # Load mappings from config
        for filename in self.mappings:
            mapping_repository.load_from_json(self.dir / filename)

    @classmethod
    def load_from_file(cls, path):
        global config
        with open(path) as cfgfile:
            data = cfgfile.read()
            raw = json.loads(data)
            config = Config(
                metamodels=raw["metamodels"], mappings=raw["mappings"], dir=path.parent
            )


config: Optional[Config] = None
