import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path
import os

import gettext

try:
    patterns_translation = gettext.translation(
        "patterns", localedir=os.environ["LOCALE_PATH"], languages=["pl"]
    )
    _ = patterns_translation.gettext
except Exception as e:
    print(e)
    _ = gettext.gettext

plt.rcParams["figure.figsize"] = (12, 12)


colors = ["violet", "red", "green", "blue", "cyan", "magenta", "crimson"]


MEASURES = ["TSA", "ASA", "FSA", "LSA", "TC", "AC", "FC", "LC"]
MEASURES_RESULTING = ["RSSA", "RC", "RS"]

FIGURE_COUNTER = 1

IMAGES_DIR = "images"
TABLES_DIR = "tables"


def eval_model_plots(modelname, data,path):
    data = pd.DataFrame(data=data[1:], columns=data[0])
    data["metamodel"] = data["metamodel"].str.upper()

    make_plots_for_model(modelname, data, path)
    make_tables_for_model(modelname, data, path)

def eval_metamodels_plots(data, path, aggregate: bool):
    fig = plt.figure()
    data = pd.DataFrame(data=data[1:], columns=data[0])

    Path(path / IMAGES_DIR).mkdir(parents=True, exist_ok=True)
    Path(path / TABLES_DIR).mkdir(parents=True, exist_ok=True)

    # Preprocessing of data format
    data["metamodel"] = data["metamodel"].str.upper()

    if aggregate:
        aggregated = data[["metamodel"] + MEASURES]
        resulting = data[["metamodel"] + MEASURES_RESULTING]

        plot_aggregated(
            path,
            aggregated,
            "results_aggregated",
            _("aggregated measures"),
            MEASURES,
        )

        plot_aggregated(
            path,
            resulting,
            "results_resulting",
            _("resulting measures"),
            MEASURES_RESULTING,
            yticks_add=1,
        )

        make_tables_for_aggregated(
            aggregated,
            MEASURES,
            path,
            "aggregated",
            _("Evaluation results for aggregated measures"),
        )

        make_tables_for_aggregated(
            resulting,
            MEASURES_RESULTING,
            path,
            "resulting",
            _("Evaluation results for resulting measures"),
        )

    else:
        data["pattern"] = data["pattern"].apply(
            lambda pattern: _(pattern.split("_")[0])
        )
        make_tables_for_model_patterns(data, path)
        make_plots_for_model_patterns(data, path, fig)


def make_plots_for_model_patterns(data, path: Path, fig):
    global FIGURE_COUNTER

    METAMODELS = data["metamodel"].unique()
    PATTERNS = list(data["pattern"].unique())

    # Miary dla każdego z wzorców
    for measure in MEASURES:

        ax = fig.add_subplot(projection="3d")
        ax.view_init(11, 68)

        k = 1
        for pattern in PATTERNS:
            tsa_pat = data[measure]

            X = range(len(METAMODELS))
            Z = tsa_pat[data["pattern"] == pattern]
            dx = 0.1
            dy = 0.1
            dz = 0.5
            Y = k + 0.4

            ax.bar3d(X, Y, 0, dx, dy, Z, alpha=1, shade=False)
            k += 1

        ax.set_yticks(range(len(PATTERNS) + 1))
        ax.set_yticklabels(PATTERNS + [""], fontsize=8, horizontalalignment="center")
        ax.set_xticks(range(len(METAMODELS)))
        ax.set_xticklabels(METAMODELS)
        plt.xlabel("$\mathcal{M}^{d}$", fontsize=20, labelpad=20)
        plt.ylabel("$\Pi_{(s,d)}$", fontsize=20, labelpad=20)
        plt.savefig(
            path / IMAGES_DIR / "{}_patterns.pdf".format(measure), bbox_inches="tight"
        )

    FIGURE_COUNTER += 1
    fig = plt.figure(FIGURE_COUNTER)


def make_plots_for_model(modelname, data, path: Path):
    
    MEASURES_ATOMS = MEASURES[:4]
    MEASURES_CONCEPTS = MEASURES[4:]


    atoms = data[["metamodel"] + MEASURES_ATOMS]
    concepts = data[["metamodel"] + MEASURES_CONCEPTS]
    resulting = data[["metamodel"] + MEASURES_RESULTING]
    
    plot_aggregated(
        path,
        atoms,
        f"{modelname}_results_atoms",
        _("miary"),
        MEASURES_ATOMS,
        modify_yticklabels=False
    )
    
    plot_aggregated(
        path,
        atoms,
        f"{modelname}_results_concepts",
        _("miary"),
        MEASURES_ATOMS,
        modify_yticklabels=False
    )
    
    plot_aggregated(
        path,
        atoms,
        f"{modelname}_resutlts_resulting",
        _("miary"),
        MEASURES_ATOMS,
        modify_yticklabels=False
    )




def plot_aggregated(path, data, filename, ylabel, yticks, yticks_add=0, modify_yticklabels = True):
    global FIGURE_COUNTER
    METAMODELS = data["metamodel"].unique()

    fig = plt.figure(FIGURE_COUNTER)
    ax = fig.add_subplot(projection="3d")
    ax.view_init(11, 68)
  #  ax.set_title(title, fontsize=28)

    k = 1
    for measure in yticks:
        X = [0, 1, 2]
        Y = k + 0.4
        Z = data[measure]
        dx = 0.1
        dy = 0.1
        dz = 0.5
        k += 1
        ax.bar3d(X, Y, 0, dx, dy, Z, alpha=1, shade=False)

    yticks = ["" for _ in range(yticks_add)] + yticks

    ax.set_yticks(range(len(yticks)))
    if modify_yticklabels:
        yticks = ["${0}_{{s,d}}$".format(a) if a else "" for a in yticks]
    ax.set_yticklabels(yticks)
    ax.set_xticks(range(len(METAMODELS)))
    ax.set_xticklabels(METAMODELS)
    plt.xlabel("$\mathcal{M}^{d}$", fontsize=20, labelpad=20)
    plt.ylabel(ylabel, fontsize=20, labelpad=20)
    plt.savefig(path / IMAGES_DIR / f"{filename}.pdf", bbox_inches="tight")
    FIGURE_COUNTER += 1


def make_tables_for_model_patterns(data, path: Path):

    PATTERNS = list(data["pattern"].unique())
    METAMODELS = data["metamodel"].unique()
    TARGET_METAMODEL = "$\mathcal{M}^d$"

    for measure in MEASURES:

        m_data = data[["metamodel", "pattern", measure]]

        def wrapped(s):
            if " " in s and len(s) > 10:
                s = s.replace(" ", "\\newline ")
            return s

        keys = [TARGET_METAMODEL] + [wrapped(_(p)) for p in PATTERNS]
        values = [list(METAMODELS)]

        measure_data = [list(m_data[m_data["pattern"] == p][measure]) for p in PATTERNS]
        values.extend(measure_data)

        df = pd.DataFrame(dict(zip(keys, values)))

        with open(path / TABLES_DIR / f"{measure}_p.tex", "w") as measure_latex_file:

            latex_table = (
                df.style.hide(axis="index")
                .format(precision=3)
                .to_latex(
                    column_format="P{0.8cm}"
                    + "P{1.2cm}" * (len(PATTERNS) - 2)
                    + "P{2.1cm}"
                    + "P{1.02cm}",
                    position="ht!",
                    hrules=True,
                    label="tab:{0}_p".format(measure),
                    caption="Wyniki ewaluacji miary ${}_p$".format(measure),
                )
            )

            measure_latex_file.write(latex_table)


def make_tables_for_aggregated(data, measures, path: Path, tabname, caption, change_column_name=True):

    METAMODELS = data["metamodel"].unique()
    TARGET_METAMODEL = "$\mathcal{M}^d$"

    if change_column_name:
        column_name = lambda c: f"${c}_{{s,d}}$" if c in measures else TARGET_METAMODEL
        data = data.rename(column_name, axis="columns")
    else:
        data = data.rename(columns={'metamodel': TARGET_METAMODEL})

    with open(path / TABLES_DIR / f"measures_{tabname}.tex", "w") as latex_file:
        latex_table = (
            data.style.hide(axis="index")
            .format(precision=3)
            .to_latex(
                position="ht!",
                position_float ="centering",
                hrules=True,
                label=f"tab:ewaluacja_miary_{tabname}",
                caption=caption,
            )
        )

        latex_file.write(latex_table)


def make_tables_for_model(modelname, data, path):

    firstlvl = data[["metamodel"] + MEASURES]
    resulting = data[["metamodel"] + MEASURES_RESULTING]


    make_tables_for_aggregated(firstlvl, MEASURES, path, f"{modelname}_firstlvl", 
        _(f"Wyniki ewaluacji translacji modelu {modelname.upper()} miarami pierwszego poziomu"),
        False)

    make_tables_for_aggregated(resulting, MEASURES_RESULTING, path, f"{modelname}_resulting", 
    _(f"Wyniki ewaluacji translacji modelu {modelname.upper()} miarami wynikowymi"),
    False)