from http.client import UnimplementedFileMode
import logging
import typer
from enum import Enum
import gettext
from pathlib import Path
from os import listdir
from os.path import isfile, join
import json
from typing import List, Optional
from tabulate import tabulate
from gettext import gettext as _

from eval_semantics.evaluation.base import (
    pattern_evaluate,
    ConceptsAggregation,
    SemanticAtomsAggregation,
    SemanticAtomsParticlesAggregation,
    ConceptsParticlesAggregation,
    FinalMeasures,
)

import eval_semantics.config as cfg
from eval_semantics.config import Config
from eval_semantics.version import __version__


def make_commands(app: typer.Typer):
    def version_callback(value: bool):
        if value:
            typer.echo(f"eval_semantics version: {__version__}")
            raise typer.Exit()

    @app.callback()
    def main(
        ctx: typer.Context,
        debug: bool = typer.Option(default=False, help=_("Show debug log")),
        version: Optional[bool] = typer.Option(
            None,
            "--version",
            help=_("Shows application version"),
            callback=version_callback,
            is_eager=True,
        ),
    ):

        ctx.debuglog = debug
        logging.getLogger().setLevel(10 if debug else 40)

    class GenerateMappingType(Enum):
        aom_eer = "aom_eer"
        aom_orm = "aom_orm"
        aom_uml = "aom_uml"

    @app.command()
    def generate_mapping(
        ctx: typer.Context,
        mapping: GenerateMappingType = typer.Argument(
            ...,
            help=_("mapping to generate"),
        ),
    ):

        mpng = None
        match mapping:
            case GenerateMappingType.aom_eer:
                from eval_semantics.mappings.aom2eer import aom2eer

                mpng = aom2eer
            case GenerateMappingType.aom_orm:
                from eval_semantics.mappings.aom2orm import aom2orm

                mpng = aom2orm
            case GenerateMappingType.aom_uml:
                from eval_semantics.mappings.aom2uml import aom2uml

                mpng = aom2uml

        data = json.dumps(json.loads(mpng.json(models_as_dict=False)), indent=4)
        typer.echo(data)

    class GenerateMetamodelType(Enum):
        aom = "aom"
        eer = "eer"
        orm = "orm"
        uml = "uml"

    @app.command()
    def generate_metamodel(
        ctx: typer.Context,
        metamodel: GenerateMetamodelType = typer.Argument(
            ..., help=_("metamodel to generate")
        ),
    ):
        mm = None

        match metamodel:
            case GenerateMetamodelType.aom:
                from eval_semantics.metamodels.aom import aom

                mm = aom
            case GenerateMetamodelType.eer:
                from eval_semantics.metamodels.eer import eer

                mm = eer
            case GenerateMetamodelType.orm:
                from eval_semantics.metamodels.orm import orm

                mm = orm
            case GenerateMetamodelType.uml:
                from eval_semantics.metamodels.uml import uml

                mm = uml

        data = json.dumps(json.loads(mm.json(models_as_dict=False)), indent=4)
        typer.echo(data)

    class GenerateModelType(Enum):
        esnm_aom = "esnm_aom"

    @app.command()
    def generate_model(
        ctx: typer.Context,
        config: Path = typer.Option(
            default=Path("config.json"),
            exists=True,
            file_okay=True,
            dir_okay=False,
            readable=True,
            resolve_path=True,
            help=_("path to config file"),
        ),
        model: GenerateModelType = typer.Argument(..., help=_("model to generate")),
    ):

        # Load config
        Config.load_from_file(config)

        m = None
        match model:
            case GenerateModelType.esnm_aom:
                from eval_semantics.models.esnm_aom import esnm_aom

                m = esnm_aom

        data = json.dumps(json.loads(m.json(models_as_dict=False)), indent=4)
        typer.echo(data)

    class TabulateFormat(Enum):
        plain = "plain"
        simple = "simple"
        grid = "grid"
        fancy_grid = "fancy_grid"
        pipe = "pipe"
        orgtbl = "orgtbl"
        latex = "latex"
        tsv = "tsv"

    @app.command()
    def eval_metamodels(
        ctx: typer.Context,
        config: Path = typer.Option(
            default=Path("config.json"),
            exists=True,
            file_okay=True,
            dir_okay=False,
            readable=True,
            resolve_path=True,
            help=_("path to config file."),
        ),
        aggregate: bool = typer.Option(default=False, help=_("Metrics type")),
        output_format: TabulateFormat = typer.Option(
            default="plain",
            help=_("output table format"),
        ),
        results_dir: Optional[Path] = typer.Option(
            default=None,
            exists=True,
            file_okay=False,
            dir_okay=True,
            help=_(
                "path where result artifacts(figures + tables for thesis) are saved"
            ),
        ),
    ):
        from eval_semantics.mappings import repository

        Config.load_from_file(config)

        data = []
        if not aggregate:
            data.append(
                [
                    "metamodel",
                    "pattern",
                    "TSA",
                    "ASA",
                    "FSA",
                    "LSA",
                    "TC",
                    "AC",
                    "FC",
                    "LC",
                ]
            )
        else:
            data.append(
                [
                    "metamodel",
                    "TSA",
                    "ASA",
                    "FSA",
                    "LSA",
                    "TC",
                    "AC",
                    "FC",
                    "LC",
                    "RSSA",
                    "RC",
                    "RS",
                ]
            )

        for pskey, pset in repository.pattern_sets.items():

            agg = []

            for pattern in pset.patterns.values():

                logging.debug("---" * 5 + "\n" + pattern.name + "\n" + "---" * 5)

                atoms, concepts = pattern_evaluate(pattern)
                agg_atoms, agg_concepts = (
                    SemanticAtomsAggregation.aggregate(atoms),
                    ConceptsAggregation.aggregate(concepts),
                )

                logging.debug(atoms)
                logging.debug(concepts)
                row = [
                    agg_atoms.TSA(),
                    agg_atoms.ASA(),
                    agg_atoms.FSA(),
                    agg_atoms.LSA(),
                    agg_concepts.TC(),
                    agg_concepts.AC(),
                    agg_concepts.FC(),
                    agg_concepts.LC(),
                ]

                for r in range(len(row)):
                    if row[r] is None:
                        row[r] = 0.0

                if not aggregate:
                    data.append(
                        [
                            pskey[1],
                            pattern.name,
                        ]
                        + list(map(lambda num: round(num, 3), row))
                    )
                else:
                    agg.append((agg_atoms, agg_concepts))

            if aggregate:
                agg_sa = SemanticAtomsParticlesAggregation.aggregate(
                    [metrices[0] for metrices in agg]
                )
                agg_c = ConceptsParticlesAggregation.aggregate(
                    [metrices[1] for metrices in agg]
                )

                final = FinalMeasures.compute(agg_sa, agg_c)

                data.append(
                    [
                        pskey[1],
                        agg_sa.TSA(),
                        agg_sa.ASA(),
                        agg_sa.FSA(),
                        agg_sa.LSA(),
                        agg_c.TC(),
                        agg_c.AC(),
                        agg_c.FC(),
                        agg_c.LC(),
                        final.rssa,
                        final.rsc,
                        final.rs,
                    ]
                )

        typer.echo(
            tabulate(
                data,
                headers="firstrow",
                floatfmt=".3f",
                tablefmt=str(output_format.value),
            )
        )

        if results_dir:
            from eval_semantics import plotting

            plotting.eval_metamodels_plots(data, results_dir, aggregate)

    @app.command()
    def eval_model(
        ctx: typer.Context,
        config: Path = typer.Option(
            default=Path("config.json"),
            exists=True,
            file_okay=True,
            dir_okay=False,
            readable=True,
            resolve_path=True,
            help=_("path to config file"),
        ),
        model: Path = typer.Argument(
            ...,
            help=_("model to load"),
            exists=True,
            file_okay=True,
            dir_okay=False,
            readable=True,
            resolve_path=True,
        ),
        output_format: TabulateFormat = typer.Option(
            default="plain",
            help=_("output table format"),
        ),        
        results_dir: Optional[Path] = typer.Option(
            default=None,
            exists=True,
            file_okay=False,
            dir_okay=True,
            help=_(
                "path where result artifacts(figures + tables for thesis) are saved"
            ),
        ),
    ):
        # Load config
        Config.load_from_file(config)

        # LoadModel
        from eval_semantics.models.base import repository as model_repository

        model = model_repository.load_from_json(model)

        # Metamodels
        from eval_semantics.metamodels.base import repository as metamodel_repository

        # Evaluation
        from eval_semantics.evaluation.model import eval_model

        data = [
            [
                "metamodel",
                "TSA",
                "ASA",
                "FSA",
                "LSA",
                "TC",
                "AC",
                "FC",
                "LC",
                "RSSA",
                "RC",
                "RS",
            ]
        ]

        for metamodel in model.mappings.keys():
            atoms, concepts, final = eval_model(model, metamodel)

            data.append(
                [
                    metamodel,
                    *atoms,  # .TSA()#, atoms.ASA(), atoms.FSA(), atoms.LSA(),
                    *concepts,  # .TC(), concepts.AC(), concepts.FC(), concepts.LC(),
                    final.rssa,
                    final.rsc,
                    final.rs,
                ]
            )

        typer.echo(
            tabulate(
                data,
                headers="firstrow",
                floatfmt=".3f",
                tablefmt=str(output_format.value),
            )
        )

        if results_dir:
            from plotting import eval_model_plots
            eval_model_plots(model.name, data,results_dir)

