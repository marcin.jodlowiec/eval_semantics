from re import M
from eval_semantics.semantics.semantics import (
    Concept,
    SemanticAtom,
    SemanticParticle,
    CategorizationSubstitution,
    ConcretizationSubstitution,
    AtomOperation,
)
from eval_semantics import pyclom
from eval_semantics.pyclom import clom
from eval_semantics.metamodels.base import MetamodelSemantics


uml = MetamodelSemantics(name="uml")

uml.define_concept("NamedElement", [])
uml.define_concept("Type", [clom["domain"]])
uml.define_concept("Classifier", [clom["classifier"]])

uml.define_concept("Class", [uml.concept("Classifier"), clom["entity type"]])

uml.define_concept("Association", [uml.concept("Classifier"), clom["association"]])

uml.define_concept(
    "AssociationClass",
    [
        uml.concept("Class"),
        uml.concept("Association"),
    ],
)


uml.define_concept("Generalization", [clom["categorization"]])

uml.define_concept("Property", [clom["attribute"], clom["role"]])

uml.define_concept("DataType", [clom["domain"], clom["classifier"]])

uml.define_concept(
    "ValueSpecification",
    [
        clom["atomic entity"],
    ],
)

uml.define_concept("Enumeration", [clom["atomic type"]])

uml.define_concept("VisibilityKind", [clom["atomic type"]])

for kind in ["package", "private", "protected", "public"]:
    uml.define_concept(kind, [uml.concept("VisibilityKind")])


uml.define_semantic_atom(
    id="1",
    is_possibilistic=True,
    verbalization="{0} can be abstract",
    concepts=[uml.concept("Classifier")],
)

uml.define_semantic_atom(
    id="2",
    is_possibilistic=True,
    verbalization="{0} can have property of shared aggregation",
    concepts=[uml.concept("Property")],
)

uml.define_semantic_atom(
    id="3",
    is_possibilistic=True,
    verbalization="{0} can have property of composed aggregation",
    concepts=[uml.concept("Property")],
)

uml.define_semantic_atom(
    id="4",
    is_possibilistic=True,
    verbalization="{0} can be identifier",
    concepts=[uml.concept("Property")],
)

uml.define_semantic_atom(
    id="5",
    is_possibilistic=True,
    verbalization="{0} can be read only",
    concepts=[uml.concept("Property")],
)

uml.define_semantic_atom(
    id="6",
    is_possibilistic=True,
    verbalization="{0} can be ordered",
    concepts=[uml.concept("Property")],
)

uml.define_semantic_atom(
    id="7",
    is_possibilistic=True,
    verbalization="{0} can be unique",
    concepts=[uml.concept("Property")],
)

uml.define_semantic_atom(
    id="8",
    is_possibilistic=True,
    verbalization="{0} can be static",
    concepts=[uml.concept("Property")],
)

uml.define_semantic_atom(
    id="9",
    is_possibilistic=True,
    verbalization="{0} set can be overlapping",
    concepts=[uml.concept("Generalization")],
)

uml.define_semantic_atom(
    id="10",
    is_possibilistic=True,
    verbalization="{0} set can be disjoint",
    concepts=[uml.concept("Generalization")],
)

uml.define_semantic_atom(
    id="11",
    is_possibilistic=True,
    verbalization="{0} can be substitutable",
    concepts=[uml.concept("Generalization")],
)

uml.define_semantic_atom(
    id="12",
    is_possibilistic=True,
    verbalization="{0} can have attribute {1}",
    concepts=[uml.concept("Class"), uml.concept("Property")],
)

uml.define_semantic_atom(
    id="13",
    is_possibilistic=True,
    verbalization="{0} can be end of {1}",
    concepts=[uml.concept("Property"), uml.concept("Association")],
)

uml.define_semantic_atom(
    id="14",
    is_possibilistic=True,
    verbalization="{0} can be end of {1}",
    concepts=[uml.concept("Association"), uml.concept("Property")],
)

uml.define_semantic_atom(
    id="15",
    is_possibilistic=True,
    verbalization="{0} can be navigable from {1}",
    concepts=[uml.concept("Property"), uml.concept("Association")],
)

uml.define_semantic_atom(
    id="16",
    is_possibilistic=True,
    verbalization="{0} can be subset of {1}",
    concepts=[
        (uml.concept("Property"), "Property_1"),
        (uml.concept("Property"), "Property_2"),
    ],
)

uml.define_semantic_atom(
    id="17",
    is_possibilistic=True,
    verbalization="{0} can have defined lower bound as {1}",
    concepts=[uml.concept("Property"), pyclom.math["integer"]],
)

uml.define_semantic_atom(
    id="18",
    is_possibilistic=True,
    verbalization="{0} can have defined upper bound as {1}",
    concepts=[uml.concept("Property"), pyclom.math["unlimited integer"]],
)

uml.define_semantic_atom(
    id="19",
    is_possibilistic=True,
    verbalization="{0} can be type for {1}",
    concepts=[uml.concept("Type"), uml.concept("Property")],
)

uml.define_semantic_atom(
    id="20",
    is_possibilistic=True,
    verbalization="{0} can have default value of {1}",
    concepts=[uml.concept("Property"), uml.concept("ValueSpecification")],
)

uml.define_semantic_atom(
    id="21",
    is_possibilistic=True,
    verbalization="{0} can be generalization in {1}",
    concepts=[uml.concept("Classifier"), uml.concept("Generalization")],
)

uml.define_semantic_atom(
    id="22",
    is_possibilistic=False,
    verbalization="{0} can be specialization in {1}",
    concepts=[uml.concept("Classifier"), uml.concept("Generalization")],
)

uml.define_semantic_atom(
    id="23",
    is_possibilistic=False,
    verbalization="{0} can have defined {1}",
    concepts=[uml.concept("NamedElement"), uml.concept("VisibilityKind")],
)


uml.define_semantic_particle(
    name="class",
    atoms=[
        uml.semantic_atom("1"),
        uml.semantic_atom("23"),
    ],
    concepts=[
        CategorizationSubstitution(
            substituting=uml.concept("Class"), substituted=uml.concept("Classifier")
        ),
        CategorizationSubstitution(
            substituting=uml.concept("Class"), substituted=uml.concept("NamedElement")
        ),
        uml.concept("VisibilityKind"),
    ],
)

uml.define_semantic_particle(
    name="attribute",
    atoms=[
        (uml.semantic_atom("12"), AtomOperation.Activation),
        (uml.semantic_atom("3"), AtomOperation.Activation),
        uml.semantic_atom("4"),
        uml.semantic_atom("5"),
        uml.semantic_atom("6"),
        uml.semantic_atom("7"),
        uml.semantic_atom("8"),
        uml.semantic_atom("17"),
        uml.semantic_atom("18"),
        uml.semantic_atom("20"),
        uml.semantic_atom("23"),
    ],
    concepts=[
        uml.concept("Class"),
        uml.concept("Property"),
        uml.concept("Type"),
        CategorizationSubstitution(
            substituting=uml.concept("Property"),
            substituted=uml.concept("NamedElement"),
            context=[uml.semantic_atom("23")],
        ),
        pyclom.math["unlimited integer"],
        pyclom.math["integer"],
        uml.concept("ValueSpecification"),
        uml.concept("VisibilityKind"),
    ],
)

uml.define_semantic_particle(
    name="association",
    atoms=[
        uml.semantic_atom("1"),
        uml.semantic_atom("23"),
    ],
    concepts=[
        CategorizationSubstitution(
            substituting=uml.concept("Association"),
            substituted=uml.concept("Classifier"),
        ),
        CategorizationSubstitution(
            substituting=uml.concept("Association"),
            substituted=uml.concept("NamedElement"),
        ),
        uml.concept("VisibilityKind"),
    ],
)

uml.define_semantic_particle(
    name="role",
    atoms=[
        (uml.semantic_atom("13"), AtomOperation.Activation),
        uml.semantic_atom("14"),
        uml.semantic_atom("15"),
        uml.semantic_atom("2"),
        uml.semantic_atom("3"),
        uml.semantic_atom("4"),
        uml.semantic_atom("5"),
        uml.semantic_atom("6"),
        uml.semantic_atom("7"),
        uml.semantic_atom("8"),
        uml.semantic_atom("16"),
        uml.semantic_atom("17"),
        uml.semantic_atom("18"),
        uml.semantic_atom("19"),
        uml.semantic_atom("23"),
    ],
    concepts=[
        uml.concept("Class"),
        uml.concept("Association"),
        uml.concept("Property"),
        uml.concept("Type"),
        CategorizationSubstitution(
            substituting=uml.concept("Property"),
            substituted=(uml.concept("Property"), "Property_1"),
            context=[uml.semantic_atom("16")],
        ),
        pyclom.math["unlimited integer"],
        pyclom.math["integer"],
        (uml.concept("Property"), "Property_2"),
        uml.concept("VisibilityKind"),
    ],
)

uml.define_semantic_particle(
    name="association class",
    atoms=[],
    particles=[
        uml.particle("class"),
        uml.particle("association"),
    ],
    concepts=[
        CategorizationSubstitution(
            substituting=uml.concept("Association"),
            substituted=uml.concept("Classifier"),
        ),
        CategorizationSubstitution(
            substituting=uml.concept("Association"),
            substituted=uml.concept("NamedElement"),
        ),
        uml.concept("VisibilityKind"),
    ],
)

uml.define_semantic_particle(
    name="generalization",
    atoms=[
        uml.semantic_atom("11"),
        (uml.semantic_atom("21"), AtomOperation.Activation),
        (uml.semantic_atom("21"), AtomOperation.Activation),
    ],
    concepts=[
        uml.concept("Generalization"),
        ConcretizationSubstitution(
            substituting=Concept(name="generalization", individual=True),
            substituted=uml.concept("Classifier"),
            context=[uml.semantic_atom("21")],
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="specialization", individual=True),
            substituted=uml.concept("Classifier"),
            context=[uml.semantic_atom("22")],
        ),
    ],
)

uml.define_semantic_particle(
    name="generalization set",
    atoms=[
        uml.semantic_atom("9"),
        uml.semantic_atom("10"),
    ],
    particles=[uml.particle("generalization")],
    concepts=[
        uml.concept("Generalization"),
        uml.particle("generalization").concept("generalization"),
        uml.particle("generalization").concept("specialization"),
    ],
)
