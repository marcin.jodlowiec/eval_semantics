<div align="center">
<img src="/assets/logo.png" align="center" width="200" alt="eval_semantics">
</div>

# eval_semantics

`eval_semantics` jest narzędziem do ewaluacji semantyki powstałej podczas translacji metamodeli oraz modeli. Narzędzie działa zgodnie z metodą opisaną w ramach rozprawy doktorskiej pt. _Metody modelowania i translacji modeli baz danych dla metamodelu asocjacyjnego_ i implementuje opracowane miary.

## Wymagania

* Python ≥ 3.10

## Instalacja i uruchomienie

Narzędzie można zainstalować samodzielnie budując moduł Python lub korzystając z przedbudowanej wersji (tylko dla windows).

### Ustawienia deweloperskie

1. Sklonowanie repozytorium z kodem i wejście do katalogu.
```bash
git clone git@gitlab.com:marcin.jodlowiec/eval_semantics.git
cd eval_semantics
```

2. Zalecane jest utworzenie [wirtualnego środowiska](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/).

```bash
python -m pip install --user virtualenv
python -m venv .venv
```

3. Aktywacja wirtualnego środowiska.

Windows:
```powershell
.\.venv\Scripts\activate.bat
```

Linux:
```bash
source .venv/bin/activate
```

4. Instalacja modułu przy użyciu `pip`.

```bash
python -m pip install .
```

### Wersja przedbudowana


Najnowsze wydanie wraz z przedbudowaną wersją oprogramowania można znaleźć pod [następującym linkiem](https://gitlab.com/marcin.jodlowiec/eval_semantics/-/releases/permalink/latest).


## Konfiguracja

Plik konfiguracyjny zdefiniowany jest w formacie JSON. Obiekt składa się z następujących kluczy:
* `metamodels` - wartością jest lista plików w formacie JSON z semantyką metamodeli,
* `mappings` - wartością jest lista plików w formacie JSON z odwzorowaniami.  

#### Przykład konfiguracji

Poniższa konfiguracja  metamodeli oraz odwzorowań rozważanych w pracy dołączona jest do projektu jako domyślna.

```json
{
   "metamodels": [
      "metamodels/aom.json",
      "metamodels/eer.json",
      "metamodels/orm.json",
      "metamodels/uml.json"
   ],
   "mappings": [
      "mappings/aom_eer.json",
      "mappings/aom_orm.json",
      "mappings/aom_uml.json"

   ]
}
```

## Użycie

Ocena semantyki metamodeli oraz modeli odbywa się przy użyciu następujących komend:
* `eval-metamodels`
* `eval-model`

### `eval-metamodels`

Komenda służy do ewaluacji semantyki metamodeli.
Paramtery:
* `--config` -- ściezka do pliku konfiguracyjnego.
* `--no-aggregate` (domyślny) -- wyniki dla miar niezagregowanych.
* `--aggregate` -- wyniki dla miar zagregowanych.
* `--help` -- wyświetlenie pomocy.
* `--output-format` -- format wyjścia (domyślnie: tsv).

#### Przykład użycia
```bash
eval_semantics eval-metamodels --aggregate
```
### `eval-model`

Komenda służy do ewaluacji semantyki modeli.
Wymagany jest argument z ścieżką do pliku z modelem.
#### Przykład użycia
```bash
eval_semantics eval-model models/esnm-aom.json
```

### Pozostałe komendy

Następujące komendy służą do wygenerowania dokumentów JSON dla reprezentacji semantycznych artefaktów rozważanych w pracy.

#### `generate-metamodel`

Komenda służy do generowania na wyjście standardowe reprezentacji semantycznych metamodeli `aom`, `eer`, `orm`, `uml`.

#### Przykład użycia

```bash
eval_semantics generate-metamodel aom
```

#### `generate-mapping`
Komenda służy do generowania na wyjście standardowe odwzorowań `aom_eer`, `aom_orm`, `aom_uml`.

```bash
eval_semantics generate-mapping aom_uml
```

#### `generate-model`
Komenda służy do generowania na wyjście standardowe zakodowanych modeli `esnm_aom`.

```bash
eval_semantics generate-model esnm_aom
```

## Autor
Marcin Jodłowiec
