from eval_semantics.semantics.semantics import (
    Concept,
    SemanticAtom,
    SemanticParticle,
    CategorizationSubstitution,
    ConcretizationSubstitution,
    AtomOperation,
)
from eval_semantics import pyclom
from eval_semantics.pyclom import clom
from eval_semantics.metamodels.base import repository, MetamodelSemantics


eer = MetamodelSemantics(name="eer")

eer.define_concept("Element", [clom["classifier"]])
eer.define_concept("EntitySet", [clom["entity type"]])
eer.define_concept("RelationshipSet", [eer.concept("Element"), clom["association"]])
eer.define_concept("Attribute", [clom["attribute"]])
eer.define_concept("ValueSet", [clom["domain"]])
eer.define_concept("Role", [clom["role"]])
eer.define_concept("Generalization/Specialization", [clom["categorization"]])
eer.define_concept("CardinalityKind", [clom["atomic type"]])
eer.define_concept("one", [eer.concept("CardinalityKind")])
eer.define_concept("many", [eer.concept("CardinalityKind")])
eer.define_concept("ParticipationKind", [clom["atomic type"]])
eer.define_concept("partial", [eer.concept("ParticipationKind")])
eer.define_concept("total", [eer.concept("ParticipationKind")])
eer.define_concept("DisjointnessKind", [clom["atomic type"]])
eer.define_concept("disjoint", [eer.concept("DisjointnessKind")])
eer.define_concept("overlapping", [eer.concept("DisjointnessKind")])


eer.define_semantic_atom(
    id="1",
    is_possibilistic=True,
    verbalization="{0} can be weak",
    concepts=[eer.concept("EntitySet")],
)

eer.define_semantic_atom(
    id="2",
    is_possibilistic=True,
    verbalization="{0} can be identifying",
    concepts=[eer.concept("RelationshipSet")],
)

eer.define_semantic_atom(
    id="3",
    is_possibilistic=True,
    verbalization="{0} can be multivalent",
    concepts=[eer.concept("Attribute")],
)

eer.define_semantic_atom(
    id="4",
    is_possibilistic=True,
    verbalization="{0} can be null",
    concepts=[eer.concept("Attribute")],
)

eer.define_semantic_atom(
    id="5",
    is_possibilistic=True,
    verbalization="{0} can be unique",
    concepts=[eer.concept("Attribute")],
)

eer.define_semantic_atom(
    id="6",
    is_possibilistic=True,
    verbalization="{0} can be complex",
    concepts=[eer.concept("Attribute")],
)

eer.define_semantic_atom(
    id="7",
    is_possibilistic=True,
    verbalization="{0} can be derived",
    concepts=[eer.concept("Attribute")],
)

eer.define_semantic_atom(
    id="8",
    is_possibilistic=True,
    verbalization="{0} can be existence dependent",
    concepts=[eer.concept("Role")],
)

eer.define_semantic_atom(
    id="9",
    is_possibilistic=True,
    verbalization="{0} can have {1}",
    concepts=[eer.concept("Element"), eer.concept("Attribute")],
)

eer.define_semantic_atom(
    id="10",
    is_possibilistic=False,
    verbalization="{0} has set {1}",
    concepts=[eer.concept("Attribute"), eer.concept("ValueSet")],
)

eer.define_semantic_atom(
    id="11",
    is_possibilistic=False,
    verbalization="{0} can be part of key {1}",
    concepts=[eer.concept("Attribute"), eer.concept("EntitySet")],
)

eer.define_semantic_atom(
    id="12",
    is_possibilistic=True,
    verbalization="{0} can have upper bound equal to {1}",
    concepts=[eer.concept("Attribute"), pyclom.math["positive integer"]],
)

eer.define_semantic_atom(
    id="13",
    is_possibilistic=True,
    verbalization="{0} can have lower bount equal to {1}",
    concepts=[eer.concept("Attribute"), pyclom.math["positive integer"]],
)

eer.define_semantic_atom(
    id="14",
    is_possibilistic=True,
    verbalization="{0} can contain {1}",
    concepts=[
        (eer.concept("Attribute"), "Attribute_1"),
        (eer.concept("Attribute"), "Attribute_2"),
    ],
)

eer.define_semantic_atom(
    id="15",
    is_possibilistic=False,
    verbalization="{0} contains {1}",
    concepts=[
        eer.concept("RelationshipSet"),
        eer.concept("Role"),
    ],
)

eer.define_semantic_atom(
    id="16",
    is_possibilistic=False,
    verbalization="{0} contains {1}",
    concepts=[
        eer.concept("Role"),
        eer.concept("ParticipationKind"),
    ],
)

eer.define_semantic_atom(
    id="17",
    is_possibilistic=False,
    verbalization="{0} contains {1}",
    concepts=[
        eer.concept("Role"),
        eer.concept("CardinalityKind"),
    ],
)

eer.define_semantic_atom(
    id="18",
    is_possibilistic=True,
    verbalization="{0} can fulfill {1}",
    concepts=[
        eer.concept("EntitySet"),
        eer.concept("Role"),
    ],
)

eer.define_semantic_atom(
    id="19",
    is_possibilistic=True,
    verbalization="{0} can play role of generalization in {1}",
    concepts=[
        eer.concept("EntitySet"),
        eer.concept("Generalization/Specialization"),
    ],
)

eer.define_semantic_atom(
    id="20",
    is_possibilistic=True,
    verbalization="{0} can play role of specialization in {1}",
    concepts=[
        eer.concept("EntitySet"),
        eer.concept("Generalization/Specialization"),
    ],
)

eer.define_semantic_atom(
    id="21",
    is_possibilistic=False,
    verbalization="{0} has defined {1}",
    concepts=[
        eer.concept("Generalization/Specialization"),
        eer.concept("DisjointnessKind"),
    ],
)

eer.define_semantic_atom(
    id="22",
    is_possibilistic=False,
    verbalization="{0} has defined {1}",
    concepts=[
        eer.concept("Generalization/Specialization"),
        eer.concept("ParticipationKind"),
    ],
)

eer.define_semantic_atom(
    id="23",
    is_possibilistic=True,
    verbalization="{0} can aggregate {1}",
    concepts=[eer.concept("EntitySet"), eer.concept("Element")],
)

##### particles

eer.define_semantic_particle(
    name="entity set",
    atoms=[eer.semantic_atom("1")],
    concepts=[eer.concept("EntitySet")],
)


eer.define_semantic_particle(
    name="relationship set",
    atoms=[eer.semantic_atom("2")],
    concepts=[eer.concept("RelationshipSet")],
)

eer.define_semantic_particle(
    name="entity set attribute",
    atoms=[
        eer.semantic_atom("3"),
        eer.semantic_atom("4"),
        eer.semantic_atom("5"),
        eer.semantic_atom("7"),
        (eer.semantic_atom("9"), AtomOperation.Activation),
        eer.semantic_atom("10"),
        eer.semantic_atom("11"),
        eer.semantic_atom("12"),
        eer.semantic_atom("13"),
    ],
    concepts=[
        eer.concept("Attribute"),
        CategorizationSubstitution(
            substituting=eer.concept("EntitySet"), substituted=eer.concept("Element")
        ),
        eer.concept("ValueSet"),
        ConcretizationSubstitution(
            substituting=Concept(name="attribute upper bound"),
            substituted=pyclom.math["positive integer"],
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="attribute lower bound"),
            substituted=pyclom.math["integer"],
        ),
    ],
)

eer.define_semantic_particle(
    name="relationship set attribute",
    atoms=[
        eer.semantic_atom("3"),
        eer.semantic_atom("4"),
        eer.semantic_atom("5"),
        eer.semantic_atom("7"),
        (eer.semantic_atom("9"), AtomOperation.Activation),
        eer.semantic_atom("10"),
        eer.semantic_atom("11"),
        eer.semantic_atom("12"),
        eer.semantic_atom("13"),
    ],
    concepts=[
        eer.concept("Attribute"),
        CategorizationSubstitution(
            substituting=eer.concept("RelationshipSet"),
            substituted=eer.concept("Element"),
        ),
        eer.concept("ValueSet"),
        ConcretizationSubstitution(
            substituting=Concept(name="attribute upper bound"),
            substituted=pyclom.math["positive integer"],
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="attribute lower bound"),
            substituted=pyclom.math["integer"],
        ),
    ],
)

eer.define_semantic_particle(
    name="complex attribute",
    atoms=[(eer.semantic_atom("6"), AtomOperation.Activation), eer.semantic_atom("14")],
    concepts=[
        ConcretizationSubstitution(
            substituting=Concept(name="superattribute"),
            substituted=eer.concept("Attribute"),
            context=[eer.semantic_atom("6")],
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="superattribute"),
            substituted=(eer.concept("Attribute"), "Attribute_1"),
            context=[eer.semantic_atom("14")],
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="subattribute"),
            substituted=(eer.concept("Attribute"), "Attribute_2"),
            context=[eer.semantic_atom("14")],
        ),
    ],
)


eer.define_semantic_particle(
    name="relationship role",
    atoms=[
        eer.semantic_atom("8"),
        eer.semantic_atom("15"),
        eer.semantic_atom("16"),
        eer.semantic_atom("17"),
        eer.semantic_atom("18"),
    ],
    concepts=[
        eer.concept("Role"),
        eer.concept("RelationshipSet"),
        eer.concept("EntitySet"),
        eer.concept("CardinalityKind"),
        eer.concept("ParticipationKind"),
    ],
)

eer.define_semantic_particle(
    name="generalization-specialization",
    atoms=[
        (eer.semantic_atom("19"), AtomOperation.Activation),
        (eer.semantic_atom("20"), AtomOperation.Activation),
        eer.semantic_atom("21"),
        eer.semantic_atom("22"),
    ],
    concepts=[
        eer.concept("Generalization/Specialization"),
        ConcretizationSubstitution(
            substituting=Concept(name="generalization"),
            substituted=eer.concept("EntitySet"),
            context=[eer.semantic_atom("19")],
        ),
        ConcretizationSubstitution(
            substituting=Concept(name="specialization"),
            substituted=eer.concept("EntitySet"),
            context=[eer.semantic_atom("20")],
        ),
        eer.concept("DisjointnessKind"),
        eer.concept("ParticipationKind"),
    ],
)

eer.define_semantic_particle(
    name="aggregation",
    atoms=[(eer.semantic_atom("23"), AtomOperation.Activation)],
    concepts=[
        eer.concept("EntitySet"),
        eer.concept("Element"),
    ],
)
