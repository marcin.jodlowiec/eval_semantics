import logging
from eval_semantics import pyclom
from pydantic import BaseModel as Base
from eval_semantics.models.particle import ModelSemanticParticle
from eval_semantics.metamodels.base import repository as metamodel_repository
from eval_semantics.semantics.semantics import (
    AtomOperation,
    ParticleAlternative,
    SemanticAtom,
    Concept,
    ConceptSubstitution,
    CategorizationSubstitution,
    ConcretizationSubstitution,
    SemanticParticle,
)
from typing import List, Union, Optional, Dict, Tuple
from functools import reduce
from operator import mul
from pathlib import Path
import json


class Empty(Base):
    pass


class EmptyConcept(Empty, Concept):
    name = "EmptyConcept"


class EmptySemanticAtom(Empty, SemanticAtom):
    id = "EmptySemanticAtom"
    verbalization = ""
    metamodel = ""
    is_possibilistic = False


class Mapping(Base):
    symbol: str
    input: Union[List[Union[SemanticAtom, Concept, ConceptSubstitution]], Empty]
    output: Union[List[Union[SemanticAtom, Concept]], Empty]
    force_alternation: bool = False
    force_translation: bool = False

    class Config:
        json_encoders = {
            Concept: Concept.serialize_name,
            SemanticAtom: SemanticAtom.serialize_name,
            ConceptSubstitution: ConceptSubstitution.serialize,
        }

    @classmethod
    def serialize(cls, mapping):
        return json.loads(mapping.json(models_as_dict=False))


class MappingPattern(Base):
    name: str
    rules: List[Mapping]
    particle: Optional[SemanticParticle]

    class Config:
        json_encoders = {
            Mapping: Mapping.serialize,
            SemanticParticle: SemanticParticle.serialize_ref,
            Concept: Concept.serialize_name,
        }

    @classmethod
    def serialize_name(cls, pattern):
        return pattern.name

    @classmethod
    def serialize(cls, pattern):
        return json.loads(pattern.json(models_as_dict=False, exclude_none=True))


class MappingPatternSet(Base):
    source: str
    target: str
    patterns: Dict[str, MappingPattern]

    class Config:
        json_encoders = {
            MappingPattern: MappingPattern.serialize,
        }


# ----------- for defining model mapping
class SemanticParticleMapping(Base):
    particle: ModelSemanticParticle
    pattern: MappingPattern
    alternative: Optional[int]

    class Config:
        json_encoders = {
            ModelSemanticParticle: ModelSemanticParticle.serialize_name,
            MappingPattern: MappingPattern.serialize_name,
        }

    @classmethod
    def serialize(cls, mapping):
        return json.loads(mapping.json(models_as_dict=False, exclude_none=True))


class MappingPatternSetRepository(object):
    def __init__(self):
        self.pattern_sets: Dict[Tuple[str, str], MappingPatternSet] = {}

    def register(self, pattern_set: MappingPatternSet):
        src = pattern_set.source
        dst = pattern_set.target
        logging.debug(f"Registering mapping ({src},{dst})...")
        self.pattern_sets[(src, dst)] = pattern_set

    def pattern_set(self, src, dest):
        return self.pattern_sets[(src, dest)]

    def load_from_json(self, path: Path):
        with open(path) as file:
            data = json.loads(file.read())
            self.load_from_dict(data)

    def load_from_dict(self, data):
        source = data["source"]
        target = data["target"]

        metamodel_src = metamodel_repository.metamodel(source)
        metamodel_dst = metamodel_repository.metamodel(target)

        def atom_operations(atoms_list):
            result = []
            for atomop in atoms_list:
                if isinstance(atomop, list):
                    atom, op = atomop
                    result.append((atom.split("_")[1], AtomOperation.from_str(op)))
            return result

        def load_particle(particle_element):
            particle_name = particle_element["name"]
            particle_model = particle_element["model"]

            particle = SemanticParticle(
                name=particle_name,
                model=particle_model,
                atoms=[],
                particles=[],
                concepts=[],
            )

            for concept in particle_element["concepts"]:
                particle.concepts.append(try_concept(concept, metamodel_dst))

            if particle_element.get("is_possibilistic", False):
                particle.is_possibilistic = True

            for subparticle in particle_element["particles"]:

                if subparticle.get("model") == target:
                    subpart = metamodel_dst.particle(subparticle["name"])
                    for atom, op in atom_operations(subparticle["atoms"]):
                        subpart = subpart.with_atom_operation(atom, op)
                    if subparticle.get("is_possibilistic"):
                        subpart.is_possibilistic = True
                    particle.particles.append(subpart)

                elif subparticle.get("alternatives"):
                    alternative = ParticleAlternative(alternatives=[])

                    for alt in subparticle.get("alternatives"):
                        if alt.get("model") == target:
                            subpart = metamodel_dst.particle(alt["name"])
                            for atom, op in atom_operations(alt["atoms"]):
                                subpart = subpart.with_atom_operation(atom, op)
                            alternative.alternatives.append(subpart)

                    particle.particles.append(alternative)

                else:
                    print("WARNING: not recognized subparticle")

            return particle

        def try_semantic_atom(mapping_element, metamodel):
            if isinstance(mapping_element, str):
                element_split = mapping_element.split("_")
                if len(element_split) == 2:
                    _, id = element_split
                    atom = metamodel.semantic_atom(id)
                    return atom
            return None

        def try_concept(mapping_element, metamodel):
            if isinstance(mapping_element, str):
                element_split = mapping_element.split(":")
                if len(element_split) == 2:
                    mm, name = element_split
                    concept = None
                    if mm == metamodel.name:
                        concept = metamodel.concept(name)
                    elif mm == pyclom.MATH_CONCEPT_TYPE:
                        try:
                            concept = pyclom.math[name]
                        except KeyError:
                            concept = pyclom.math["number"](name)
                    return concept
            elif isinstance(mapping_element, list):
                concept, label = mapping_element
                return (try_concept(concept, metamodel), label)
            elif isinstance(mapping_element, dict):  # subst
                substituted = try_concept(mapping_element["substituted"], metamodel)
                context = [
                    try_semantic_atom(el, metamodel)
                    for el in mapping_element.get("context", [])
                ]

                match mapping_element["kind"]:
                    case "concretization":
                        return ConcretizationSubstitution(
                            substituting=Concept(name=mapping_element["substituting"]),
                            substituted=substituted,
                            context=context,
                        )
                    case "categorization":
                        return CategorizationSubstitution(
                            substituting=try_concept(
                                mapping_element["substituting"], metamodel
                            ),
                            substituted=substituted,
                            context=context,
                        )

            return None

        def try_none(mapping_element):
            match mapping_element:
                case "_EmptySemanticAtom":
                    return EmptySemanticAtom()
                case "EmptyConcept":
                    return EmptyConcept()

        patterns = {}
        for pat_name in data["patterns"].keys():
            pat_data = data["patterns"][pat_name]

            rules = []
            for rule in pat_data["rules"]:

                input = []
                output = []

                if isinstance(rule["input"], list):
                    for element in rule["input"]:

                        maybe_atom = try_semantic_atom(element, metamodel_src)
                        maybe_concept = try_concept(element, metamodel_src)

                        if maybe_atom is not None:
                            input.append(maybe_atom)
                        elif maybe_concept is not None:
                            input.append(maybe_concept)
                        else:
                            input.append(try_none(element))
                else:
                    input.append(try_none(rule["input"]))

                if isinstance(rule["output"], list):
                    for element in rule["output"]:

                        maybe_atom = try_semantic_atom(element, metamodel_dst)
                        maybe_concept = try_concept(element, metamodel_dst)

                        if maybe_atom is not None:
                            output.append(maybe_atom)
                        elif maybe_concept is not None:
                            output.append(maybe_concept)
                        else:
                            output.append(try_none(element))
                else:
                    output.append(try_none(rule["output"]))

                if len(input) == 1 and isinstance(input[0], Empty):
                    input = input[0]

                if len(output) == 1 and isinstance(output[0], Empty):
                    output = output[0]

                rules.append(
                    Mapping(
                        symbol=rule["symbol"],
                        input=input,
                        output=output,
                        force_alternation=rule["force_alternation"],
                        force_translation=rule["force_translation"],
                    )
                )

            pattern = MappingPattern(
                name=pat_data["name"],
                rules=rules,
                particle=load_particle(pat_data["particle"]),
            )
            patterns[pat_name] = pattern

        pattern_set = MappingPatternSet(source=source, target=target, patterns=patterns)

        self.register(pattern_set)


repository = MappingPatternSetRepository()
