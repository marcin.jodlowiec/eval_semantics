from pydantic import BaseModel as Base
from typing import Optional, ForwardRef

CLOM_CONCEPT_TYPE = "clom"

ConceptRef = ForwardRef("Concept")
RoleRef = ForwardRef("Role")
VerbRef = ForwardRef("VerbRef")


class Dictionary(Base):
    concepts: dict[str, ConceptRef] = {}

    def add_concept(self, concept: ConceptRef):
        concept.type = CLOM_CONCEPT_TYPE
        self.concepts[concept.name] = concept

    def __iadd__(self, concept: ConceptRef):
        self.add_concept(concept)
        return self

    def __getitem__(self, name):
        return self.concepts[name]


class Concept(Base):
    name: str
    type: Optional[str]
    general: Optional[list[ConceptRef]]

    def __hash__(self):
        return hash(str(self))


class Role(Concept):
    concept: Concept = []
    verb: Optional[VerbRef]

    def concept(c: Concept) -> RoleRef:
        return Role(name=c.name, concept=c)

    def with_name(name: str, c: Concept) -> RoleRef:
        return Role(name=name, concept=c)


class Verb(Concept):
    roles: list[Role]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        for role in self.roles:
            role.verb = self

    def __str__(self):
        labels = {role.name: role.concept for role in self.roles}
        return self.name.format(**labels)


class PartWhole(Verb):
    def __init__(self, part_concept, whole_concept, **kwargs):
        roles: list[Role] = [
            Role.with_name("whole", whole_concept),
            Role.with_name("part", part_concept),
        ]
        super().__init__(name="{whole} has {part}", roles=roles)


class Noun(Concept):
    individual: bool = False

    roles: Optional[list[Role]]

    def __str__(self):
        return self.name

    def __repr__(self):
        if self.type != None:
            return self.type + ":" + self.name

    @classmethod
    def serialize(cls, noun):
        return repr(noun)


#### Core

core = Dictionary()
core += Noun(name="modeling concept")
core += Noun(name="classifier", general=[core["modeling concept"]])
core += Noun(name="feature", general=[core["modeling concept"]])
core += Noun(name="relationship type", general=[core["modeling concept"]])
core += Noun(name="collection", general=[core["relationship type"]])
core += Noun(name="reference", general=[core["modeling concept"]])
core += Noun(name="characteristic", general=[core["modeling concept"]])
core += Noun(name="modeling concept characteristic", general=[core["characteristic"]])
core += Noun(name="visibility", general=[core["modeling concept characteristic"]])
core += Noun(
    name="relationship participation", general=[core["modeling concept characteristic"]]
)
core += Noun(name="name", general=[core["modeling concept characteristic"]])
core += Noun(name="identification", general=[core["modeling concept characteristic"]])
core += Noun(name="derivability", general=[core["modeling concept characteristic"]])
core += Noun(name="instance", general=[core["modeling concept"]])

classifiers = Dictionary()
classifiers += Noun(name="classifier characteristic", general=[core["characteristic"]])
classifiers += Noun(
    name="non-instantiability", general=[classifiers["classifier characteristic"]]
)
classifiers += Noun(
    name="identifier", general=[classifiers["classifier characteristic"]]
)
classifiers += Noun(name="predefined identifier", general=[classifiers["identifier"]])


features = Dictionary()
features += Noun(name="feature characteristic", general=[core["characteristic"]])
features += Noun(name="inheritability", general=[features["feature characteristic"]])
features += Noun(name="staticity", general=[features["feature characteristic"]])

collections = Dictionary()
collections += Noun(
    name="collection characteristic", general=[features["feature characteristic"]]
)
collections += Noun(
    name="repeatability", general=[collections["collection characteristic"]]
)
collections += Noun(name="uniqueness", general=[collections["repeatability"]])
collections += Noun(
    name="non-referenceability", general=[collections["collection characteristic"]]
)
collections += Noun(
    name="mutability", general=[collections["collection characteristic"]]
)
collections += Noun(name="order", general=[collections["collection characteristic"]])
collections += Noun(
    name="collection upper bound", general=[collections["collection characteristic"]]
)
collections += Noun(
    name="collection lower bound", general=[collections["collection characteristic"]]
)

entities = Dictionary()
entities += Noun(name="attribute", general=[core["feature"]])
entities += Noun(name="entity type", general=[core["classifier"]])
entities += Noun(name="entity", general=[core["instance"]])
entities += Noun(name="atomic entity", general=[entities["entity"]])
entities += Noun(name="atomic type", general=[entities["entity type"]])
entities += Noun(name="attribute characteristic", general=[core["characteristic"]])
entities += Noun(name="nullability", general=[entities["attribute characteristic"]])
entities += Noun(name="domain", general=[entities["attribute characteristic"]])
entities += Noun(name="default value", general=[entities["attribute characteristic"]])
entities += Noun(
    name="structural complexity", general=[entities["attribute characteristic"]]
)
entities += Noun(name="multivalency", general=[entities["attribute characteristic"]])


categorization = Dictionary()
categorization += Noun(name="categorization", general=[core["relationship type"]])
categorization += Noun(
    name="categorization characteristic", general=[core["characteristic"]]
)
categorization += Noun(
    name="virtuality mode", general=[categorization["categorization characteristic"]]
)
categorization += Noun(
    name="coverage", general=[categorization["categorization characteristic"]]
)
categorization += Noun(
    name="disjointness", general=[categorization["categorization characteristic"]]
)
categorization += Noun(
    name="categorization aspect",
    general=[categorization["categorization characteristic"]],
)
categorization += Noun(
    name="inheritance and polymorphism separation",
    general=[categorization["categorization characteristic"]],
)
categorization += Noun(name="inheritance")
categorization += Noun(name="polymorphism")


associations = Dictionary()
associations += Noun(
    name="association", general=[core["relationship type"], core["classifier"]]
)
associations += Noun(name="relationship", general=[core["instance"]])
associations += Noun(name="role", general=[core["feature"], core["classifier"]])
associations += Noun(name="role instance", general=[core["instance"]])
associations += Noun(name="role characteristic", general=[core["characteristic"]])
associations += Noun(
    name="n-ary role constraint", general=[associations["role characteristic"]]
)
associations += Noun(
    name="logical constraint", general=[associations["n-ary role constraint"]]
)
associations += Noun(
    name="binary role constraint", general=[associations["role characteristic"]]
)
associations += Noun(
    name="subset constraint", general=[associations["binary role constraint"]]
)
associations += Noun(
    name="unary role constraint", general=[associations["role characteristic"]]
)
associations += Noun(
    name="fulfilling category constraint",
    general=[associations["unary role constraint"]],
)
associations += Noun(
    name="existence dependency", general=[associations["unary role constraint"]]
)
associations += Noun(
    name="navigability", general=[associations["unary role constraint"]]
)
associations += Noun(
    name="multiplicity", general=[associations["unary role constraint"]]
)
associations += Noun(
    name="semantic direction", general=[associations["unary role constraint"]]
)


modeling = Dictionary()
modeling += Noun(name="model")


class Clom(object):
    def __init__(self):
        self.modeling = modeling
        self.core = core
        self.entities = entities
        self.collections = collections
        self.associations = associations
        self.categorization = categorization

    def __getitem__(self, key):
        for modname, module in self.__dict__.items():
            for name, concept in module.concepts.items():
                if key == name:
                    return concept

        raise KeyError(f'Could not find concept "{key}"')


clom = Clom()


MATH_CONCEPT_TYPE = "math"

# Numbers
math = dict()

for concept in [
    Noun(name="real", individual=False, type=MATH_CONCEPT_TYPE),
    Noun(name="integer", individual=False, type=MATH_CONCEPT_TYPE),
    Noun(name="positive integer", individual=False, type=MATH_CONCEPT_TYPE),
    Noun(name="unlimited integer", individual=False, type=MATH_CONCEPT_TYPE),
]:
    math[concept.name] = concept


import re


def make_concept_number(num):
    num = str(num)
    noun = Noun(name=str(num), individual=True, type=MATH_CONCEPT_TYPE, general=[])
    if num == "*":
        noun.general.append(math["unlimited integer"])
    elif re.match(r"[-+]?(?:\d*\.\d+|\d+)", num):
        noun.general.append(math["real"])
    elif re.match(r"\d+", num):
        if num > 0:
            noun.general.append(math["positive integer"])
        if num >= 0:
            noun.general.append(math["integer"])

    return noun


math["number"] = make_concept_number
