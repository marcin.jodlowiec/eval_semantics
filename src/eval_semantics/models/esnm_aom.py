from eval_semantics import pyclom
from .base import ModelSemantics
from eval_semantics.metamodels.base import repository as metamodel_repository
from eval_semantics.mappings import SemanticParticleMapping

from eval_semantics.semantics.semantics import (
    ConcretizationSubstitution,
    AtomOperation,
    Concept,
)

aom = metamodel_repository.metamodel("aom")


esnm_aom = ModelSemantics(metamodel="aom", name="esnm")

######################
#       OPERAND      #
######################

esnm_aom.define_concept("OPERAND (Coll)", aom.concept("Coll"))
esnm_aom.define_concept("Operand (Assoc)", aom.concept("Assoc"))
esnm_aom.define_concept("Operand's operand (Role)", aom.concept("Role"))

esnm_aom.define_semantic_particle(
    "Operand",
    aom.particle("bact"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("OPERAND (Coll)"),
            substituted=aom.particle("bact").concept("data aspect"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Operand (Assoc)"),
            substituted=aom.particle("bact").concept("relationship aspect"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Operand's operand (Role)"),
            substituted=aom.particle("bact").concept("unbreakable connection"),
        ),
    ],
    [(aom.semantic_atom("1"), AtomOperation.Activation)],
)


######################
#       OPERATOR      #
######################

esnm_aom.define_concept("OPERATOR (Coll)", aom.concept("Coll"))
esnm_aom.define_concept("Operator (Assoc)", aom.concept("Assoc"))
esnm_aom.define_concept("Operator's operator (Role)", aom.concept("Role"))

esnm_aom.define_semantic_particle(
    "Operator",
    aom.particle("bact"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("OPERATOR (Coll)"),
            substituted=aom.particle("bact").concept("data aspect"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Operator (Assoc)"),
            substituted=aom.particle("bact").concept("relationship aspect"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Operator's operator (Role)"),
            substituted=aom.particle("bact").concept("unbreakable connection"),
        ),
    ],
    [(aom.semantic_atom("1"), AtomOperation.Deactivation)],
)

###############################
#       SUBSTITUTABILITY      #
###############################

esnm_aom.define_concept("SUBSTITUTABILITY (Coll)", aom.concept("Coll"))
esnm_aom.define_concept("Substitutability (Assoc)", aom.concept("Assoc"))
esnm_aom.define_concept(
    "Substitutability's substitutability (Role)", aom.concept("Role")
)

esnm_aom.define_semantic_particle(
    "Substitutability",
    aom.particle("bact"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("SUBSTITUTABILITY (Coll)"),
            substituted=aom.particle("bact").concept("data aspect"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Substitutability (Assoc)"),
            substituted=aom.particle("bact").concept("relationship aspect"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Substitutability's substitutability (Role)"),
            substituted=aom.particle("bact").concept("unbreakable connection"),
        ),
    ],
    [(aom.semantic_atom("1"), AtomOperation.Deactivation)],
)

#########################
#   Concept
#########################

esnm_aom.define_concept("CONCEPT (Coll)", aom.concept("Coll"))

esnm_aom.define_semantic_particle(
    "Concept",
    aom.particle("collection"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("CONCEPT (Coll)"),
            substituted=aom.concept("Coll"),
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Activation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)

esnm_aom.define_concept("NET (Coll)", aom.concept("Coll"))

esnm_aom.define_semantic_particle(
    "Net (Coll)",
    aom.particle("collection"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("NET (Coll)"), substituted=aom.concept("Coll")
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Deactivation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)

esnm_aom.define_concept("QUANTIFIER (Coll)", aom.concept("Coll"))

esnm_aom.define_semantic_particle(
    "Quantifier",
    aom.particle("collection"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("QUANTIFIER (Coll)"),
            substituted=aom.concept("Coll"),
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Deactivation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)

#####
#####   Associations
#####

esnm_aom.define_concept("Instance (Assoc)", aom.concept("Assoc"))

esnm_aom.define_semantic_particle(
    "Instance",
    aom.particle("association"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Instance (Assoc)"),
            substituted=aom.concept("Assoc"),
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Activation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)

esnm_aom.define_concept("OperandNode (Assoc)", aom.concept("Assoc"))

esnm_aom.define_semantic_particle(
    "OperandNode",
    aom.particle("association"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("OperandNode (Assoc)"),
            substituted=aom.concept("Assoc"),
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Deactivation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)

esnm_aom.define_concept("OperandInstance (Assoc)", aom.concept("Assoc"))

esnm_aom.define_semantic_particle(
    "OperandInstance",
    aom.particle("association"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("OperandInstance (Assoc)"),
            substituted=aom.concept("Assoc"),
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Deactivation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)

esnm_aom.define_concept("OperandOperator (Assoc)", aom.concept("Assoc"))

esnm_aom.define_semantic_particle(
    "OperandOperator",
    aom.particle("association"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("OperandOperator (Assoc)"),
            substituted=aom.concept("Assoc"),
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Deactivation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)

esnm_aom.define_concept("OperandNet (Assoc)", aom.concept("Assoc"))

esnm_aom.define_semantic_particle(
    "OperandNet",
    aom.particle("association"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("OperandNet (Assoc)"),
            substituted=aom.concept("Assoc"),
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Deactivation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)

esnm_aom.define_concept("Modifier (Assoc)", aom.concept("Assoc"))

esnm_aom.define_semantic_particle(
    "Modifier",
    aom.particle("association"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Modifier (Assoc)"),
            substituted=aom.concept("Assoc"),
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Deactivation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)

esnm_aom.define_concept("Net (Assoc)", aom.concept("Assoc"))

esnm_aom.define_semantic_particle(
    "Net",
    aom.particle("association"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Net (Assoc)"),
            substituted=aom.concept("Assoc"),
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Deactivation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)

esnm_aom.define_concept("Fact (Assoc)", aom.concept("Assoc"))

esnm_aom.define_semantic_particle(
    "Fact",
    aom.particle("association"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Fact (Assoc)"),
            substituted=aom.concept("Assoc"),
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Deactivation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)


esnm_aom.define_concept("Conclusion (Assoc)", aom.concept("Assoc"))

esnm_aom.define_semantic_particle(
    "Conclusion",
    aom.particle("association"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Conclusion (Assoc)"),
            substituted=aom.concept("Assoc"),
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Deactivation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)

esnm_aom.define_concept("Rule (Assoc)", aom.concept("Assoc"))

esnm_aom.define_semantic_particle(
    "Rule",
    aom.particle("association"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Rule (Assoc)"),
            substituted=aom.concept("Assoc"),
        )
    ],
    [
        (aom.semantic_atom("1"), AtomOperation.Deactivation),
        (aom.semantic_atom("2"), AtomOperation.Deactivation),
    ],
)

########### Attributes ##########


def define_aom_attr(
    name, coll_concept, data_type, default_value, multiplicity, operations
):
    attr = esnm_aom.define_concept(f"{name} (Attr)", aom.concept("Attr"))

    esnm_aom.define_semantic_particle(
        name,
        aom.particle("attribute"),
        [
            ConcretizationSubstitution(
                substituting=attr, substituted=aom.concept("Attr")
            ),
            ConcretizationSubstitution(
                substituting=coll_concept, substituted=aom.concept("Coll")
            ),
            ConcretizationSubstitution(
                substituting=default_value, substituted=aom.concept("Value")
            ),
            ConcretizationSubstitution(
                substituting=data_type, substituted=aom.concept("DataType")
            ),
            ConcretizationSubstitution(
                substituting=multiplicity,
                substituted=aom.particle("attribute").concept("attribute multiplicity"),
            ),
        ],
        operations,
    )


define_aom_attr(
    name="Concept's name",
    coll_concept=esnm_aom.concept("CONCEPT (Coll)"),
    default_value=Concept(name="empty unicode", individual=True),
    data_type=aom.concept("unicode-string"),
    multiplicity=pyclom.math["number"](32),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Concept's symbol",
    coll_concept=esnm_aom.concept("CONCEPT (Coll)"),
    default_value=Concept(name="empty unicode", individual=True),
    data_type=aom.concept("unicode-string"),
    multiplicity=pyclom.math["number"](8),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Concept's emotion",
    coll_concept=esnm_aom.concept("CONCEPT (Coll)"),
    default_value=pyclom.math["number"](0.0),
    data_type=aom.concept("float"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Concept's usage",
    coll_concept=esnm_aom.concept("CONCEPT (Coll)"),
    default_value=pyclom.math["number"](0),
    data_type=aom.concept("int64"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Concept's type",
    coll_concept=esnm_aom.concept("CONCEPT (Coll)"),
    default_value=Concept(name="empty ascii", individual=True),
    data_type=aom.concept("ascii"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Concept's core concept",
    coll_concept=esnm_aom.concept("CONCEPT (Coll)"),
    default_value=pyclom.math["number"](0),
    data_type=aom.concept("int8"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Operand's type",
    coll_concept=esnm_aom.concept("OPERAND (Coll)"),
    default_value=Concept(name="empty ascii"),
    data_type=aom.concept("ascii"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Operand's variable",
    coll_concept=esnm_aom.concept("OPERAND (Coll)"),
    default_value=Concept(name="empty ascii"),
    data_type=aom.concept("ascii"),
    multiplicity=pyclom.math["number"](6),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Operand's margin type",
    coll_concept=esnm_aom.concept("OPERAND (Coll)"),
    default_value=Concept(name="empty ascii"),
    data_type=aom.concept("ascii"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Operand's role",
    coll_concept=esnm_aom.concept("OPERAND (Coll)"),
    default_value=Concept(name="null reference", individual=True),
    data_type=aom.concept("reference"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Operand's cardinality max",
    coll_concept=esnm_aom.concept("OPERAND (Coll)"),
    default_value=pyclom.math["number"](0),
    data_type=aom.concept("int64"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Operand's cardinality min",
    coll_concept=esnm_aom.concept("OPERAND (Coll)"),
    default_value=pyclom.math["number"](0),
    data_type=aom.concept("int64"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Operand's margin max",
    coll_concept=esnm_aom.concept("OPERAND (Coll)"),
    default_value=pyclom.math["number"](0.0),
    data_type=aom.concept("double"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Operand's margin min",
    coll_concept=esnm_aom.concept("OPERAND (Coll)"),
    default_value=pyclom.math["number"](0.0),
    data_type=aom.concept("double"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Operator's type",
    coll_concept=esnm_aom.concept("OPERATOR (Coll)"),
    default_value=Concept(name="empty ascii", individual=True),
    data_type=aom.concept("ascii"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Substitutability's depth",
    coll_concept=esnm_aom.concept("SUBSTITUTABILITY (Coll)"),
    default_value=pyclom.math["number"](0),
    data_type=aom.concept("int8"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Substitutability's cfdf",
    coll_concept=esnm_aom.concept("SUBSTITUTABILITY (Coll)"),
    default_value=pyclom.math["number"](0.0),
    data_type=aom.concept("float"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Substitutability's int. quant. df",
    coll_concept=esnm_aom.concept("SUBSTITUTABILITY (Coll)"),
    default_value=pyclom.math["number"](0.0),
    data_type=aom.concept("float"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Substitutability's space quant. df",
    coll_concept=esnm_aom.concept("SUBSTITUTABILITY (Coll)"),
    default_value=pyclom.math["number"](0.0),
    data_type=aom.concept("float"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)
define_aom_attr(
    name="Substitutability's time quant. df",
    coll_concept=esnm_aom.concept("SUBSTITUTABILITY (Coll)"),
    default_value=pyclom.math["number"](0.0),
    data_type=aom.concept("float"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Net's name",
    coll_concept=esnm_aom.concept("NET (Coll)"),
    default_value=Concept(name="empty unicode", individual=True),
    data_type=aom.concept("unicode-string"),
    multiplicity=pyclom.math["number"](32),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Net's type",
    coll_concept=esnm_aom.concept("NET (Coll)"),
    default_value=Concept(name="empty ascii", individual=True),
    data_type=aom.concept("ascii"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Net's description",
    coll_concept=esnm_aom.concept("NET (Coll)"),
    default_value=Concept(name="empty unicode", individual=True),
    data_type=aom.concept("unicode-string"),
    multiplicity=pyclom.math["number"](128),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Quantifier's cf",
    coll_concept=esnm_aom.concept("QUANTIFIER (Coll)"),
    default_value=pyclom.math["number"](0.0),
    data_type=aom.concept("float"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Quantifier's time quant.",
    coll_concept=esnm_aom.concept("QUANTIFIER (Coll)"),
    default_value=pyclom.math["number"](0.0),
    data_type=aom.concept("float"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Quantifier's space quant.",
    coll_concept=esnm_aom.concept("QUANTIFIER (Coll)"),
    default_value=pyclom.math["number"](0.0),
    data_type=aom.concept("float"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)

define_aom_attr(
    name="Quantifier's int. quant.",
    coll_concept=esnm_aom.concept("QUANTIFIER (Coll)"),
    default_value=pyclom.math["number"](0.0),
    data_type=aom.concept("float"),
    multiplicity=pyclom.math["number"](1),
    operations=[
        (aom.semantic_atom("3"), AtomOperation.Activation),
        (aom.semantic_atom("4"), AtomOperation.Deactivation),
    ],
)


def define_aom_role(name, assoc_concept, end_concept, mults, operations):
    role = esnm_aom.define_concept(f"{name} (Role)", aom.concept("Role"))

    esnm_aom.define_semantic_particle(
        name,
        aom.particle("role"),
        [
            ConcretizationSubstitution(
                substituting=role, substituted=aom.concept("Role")
            ),
            ConcretizationSubstitution(
                substituting=assoc_concept, substituted=aom.concept("Assoc")
            ),
            ConcretizationSubstitution(
                substituting=end_concept,
                substituted=aom.particle("role").concept("role end"),
            ),
            ConcretizationSubstitution(
                substituting=mults[0],
                substituted=aom.particle("role").concept("mult supreme in owner"),
            ),
            ConcretizationSubstitution(
                substituting=mults[1],
                substituted=aom.particle("role").concept("mult inf in owner"),
            ),
            ConcretizationSubstitution(
                substituting=mults[2],
                substituted=aom.particle("role").concept("mult supreme in dest"),
            ),
            ConcretizationSubstitution(
                substituting=mults[3],
                substituted=aom.particle("role").concept("mult inf in dest"),
            ),
        ],
        operations,
    )


define_aom_role(
    name="Instance's concept",
    assoc_concept=esnm_aom.concept("Instance (Assoc)"),
    end_concept=esnm_aom.concept("CONCEPT (Coll)"),
    mults=[
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"](1),
        pyclom.math["number"](0),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Deactivation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Deactivation),
        (aom.semantic_atom("10"), AtomOperation.Activation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="Instance's substitutability",
    assoc_concept=esnm_aom.concept("Instance (Assoc)"),
    end_concept=esnm_aom.concept("Substitutability (Assoc)"),
    mults=[
        pyclom.math["number"](1),
        pyclom.math["number"](0),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Deactivation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Deactivation),
        (aom.semantic_atom("10"), AtomOperation.Activation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="Operand's modifier",
    assoc_concept=esnm_aom.concept("Operand (Assoc)"),
    end_concept=esnm_aom.concept("Modifier (Assoc)"),
    mults=[
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Activation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Activation),
        (aom.semantic_atom("10"), AtomOperation.Deactivation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="Operator's operand",
    assoc_concept=esnm_aom.concept("Operator (Assoc)"),
    end_concept=esnm_aom.concept("Operand (Assoc)"),
    mults=[
        pyclom.math["number"]("*"),
        pyclom.math["number"](1),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Deactivation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Deactivation),
        (aom.semantic_atom("10"), AtomOperation.Activation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="Operator's modifier",
    assoc_concept=esnm_aom.concept("Operator (Assoc)"),
    end_concept=esnm_aom.concept("Modifier (Assoc)"),
    mults=[
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Activation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Activation),
        (aom.semantic_atom("10"), AtomOperation.Deactivation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="OperandOperator's operator",
    assoc_concept=esnm_aom.concept("OperandOperator (Assoc)"),
    end_concept=esnm_aom.concept("Operator (Assoc)"),
    mults=[
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"](1),
        pyclom.math["number"](0),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Activation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Activation),
        (aom.semantic_atom("10"), AtomOperation.Deactivation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="OperandNet's net",
    assoc_concept=esnm_aom.concept("OperandNet (Assoc)"),
    end_concept=esnm_aom.concept("Net (Assoc)"),
    mults=[
        pyclom.math["number"](0),
        pyclom.math["number"](1),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Activation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Activation),
        (aom.semantic_atom("10"), AtomOperation.Deactivation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="Net's net",
    assoc_concept=esnm_aom.concept("Net (Assoc)"),
    end_concept=esnm_aom.concept("NET (Coll)"),
    mults=[
        pyclom.math["number"](1),
        pyclom.math["number"](1),
        pyclom.math["number"](1),
        pyclom.math["number"](0),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Deactivation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Deactivation),
        (aom.semantic_atom("10"), AtomOperation.Activation),
        (aom.semantic_atom("11"), AtomOperation.Activation),
        (aom.semantic_atom("12"), AtomOperation.Activation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="Net's conceptDef",
    assoc_concept=esnm_aom.concept("Net (Assoc)"),
    end_concept=esnm_aom.concept("CONCEPT (Coll)"),
    mults=[
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"](1),
        pyclom.math["number"](0),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Deactivation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Deactivation),
        (aom.semantic_atom("10"), AtomOperation.Activation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="Net's subnet",
    assoc_concept=esnm_aom.concept("Net (Assoc)"),
    end_concept=esnm_aom.concept("Net (Assoc)"),
    mults=[
        pyclom.math["number"](1),
        pyclom.math["number"](0),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Deactivation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Deactivation),
        (aom.semantic_atom("10"), AtomOperation.Activation),
        (aom.semantic_atom("11"), AtomOperation.Activation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="Fact's net",
    assoc_concept=esnm_aom.concept("Fact (Assoc)"),
    end_concept=esnm_aom.concept("Net (Assoc)"),
    mults=[
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Deactivation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Deactivation),
        (aom.semantic_atom("10"), AtomOperation.Activation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="Conclusion's fact",
    assoc_concept=esnm_aom.concept("Conclusion (Assoc)"),
    end_concept=esnm_aom.concept("Fact (Assoc)"),
    mults=[
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Deactivation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Deactivation),
        (aom.semantic_atom("10"), AtomOperation.Activation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="Conclusion's rule",
    assoc_concept=esnm_aom.concept("Conclusion (Assoc)"),
    end_concept=esnm_aom.concept("Rule (Assoc)"),
    mults=[
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Deactivation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Deactivation),
        (aom.semantic_atom("10"), AtomOperation.Activation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="Rule's net",
    assoc_concept=esnm_aom.concept("Rule (Assoc)"),
    end_concept=esnm_aom.concept("Net (Assoc)"),
    mults=[
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Deactivation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Deactivation),
        (aom.semantic_atom("10"), AtomOperation.Activation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

define_aom_role(
    name="Modifier's modifier",
    assoc_concept=esnm_aom.concept("Modifier (Assoc)"),
    end_concept=esnm_aom.concept("Modifier (Assoc)"),
    mults=[
        pyclom.math["number"](1),
        pyclom.math["number"](1),
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Activation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Activation),
        (aom.semantic_atom("10"), AtomOperation.Deactivation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)


define_aom_role(
    name="Modifier's concept",
    assoc_concept=esnm_aom.concept("Modifier (Assoc)"),
    end_concept=esnm_aom.concept("CONCEPT (Coll)"),
    mults=[
        pyclom.math["number"]("*"),
        pyclom.math["number"]("*"),
        pyclom.math["number"](1),
        pyclom.math["number"](0),
    ],
    operations=[
        (aom.semantic_atom("5"), AtomOperation.Deactivation),
        (aom.semantic_atom("6"), AtomOperation.Activation),
        (aom.semantic_atom("7"), AtomOperation.Activation),
        (aom.semantic_atom("8"), AtomOperation.Deactivation),
        (aom.semantic_atom("9"), AtomOperation.Activation),
        (aom.semantic_atom("10"), AtomOperation.Deactivation),
        (aom.semantic_atom("11"), AtomOperation.Deactivation),
        (aom.semantic_atom("12"), AtomOperation.Deactivation),
        (aom.semantic_atom("25"), AtomOperation.Deactivation),
        (aom.semantic_atom("24"), AtomOperation.Deactivation),
    ],
)

esnm_aom.define_semantic_particle(
    "Description of Modifier",
    aom.particle("association description"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("QUANTIFIER (Coll)"),
            substituted=aom.concept("Coll"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Modifier (Assoc)"),
            substituted=aom.concept("Coll"),
        ),
    ],
)


esnm_aom.define_semantic_particle(
    "Description of Operator's operator",
    aom.particle("role description"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("QUANTIFIER (Coll)"),
            substituted=aom.concept("Coll"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Operator's operator (Role)"),
            substituted=aom.concept("Role"),
        ),
    ],
)

esnm_aom.define_semantic_particle(
    "Description of Operands's operand",
    aom.particle("role description"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("QUANTIFIER (Coll)"),
            substituted=aom.concept("Coll"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Operand's operand (Role)"),
            substituted=aom.concept("Role"),
        ),
    ],
)


esnm_aom.define_semantic_particle(
    "Description of Substitutability's substitutability",
    aom.particle("role description"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("SUBSTITUTABILITY (Coll)"),
            substituted=aom.concept("Coll"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Substitutability's substitutability (Role)"),
            substituted=aom.concept("Role"),
        ),
    ],
)

esnm_aom.define_semantic_particle(
    "OperandNode's inheritance from Operand",
    aom.particle("inheritance"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("OperandNode (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_1"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Operand (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_2"),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("natural"), substituted=aom.concept("Virtuality")
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "member inheritance aspect"
            ),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "fulfill roles inheritance aspect"
            ),
        ),
    ],
)

esnm_aom.define_semantic_particle(
    "OperandInstance’s inheritance from Operand",
    aom.particle("inheritance"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("OperandInstance (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_1"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Operand (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_2"),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("natural"), substituted=aom.concept("Virtuality")
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "member inheritance aspect"
            ),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "fulfill roles inheritance aspect"
            ),
        ),
    ],
)

esnm_aom.define_semantic_particle(
    "OperandOperator's inheritance from Operand",
    aom.particle("inheritance"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("OperandOperator (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_1"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Operand (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_2"),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("natural"), substituted=aom.concept("Virtuality")
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "member inheritance aspect"
            ),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "fulfill roles inheritance aspect"
            ),
        ),
    ],
)

esnm_aom.define_semantic_particle(
    "OperandNet's inheritance from Operand",
    aom.particle("inheritance"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("OperandNet (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_1"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Operand (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_2"),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("natural"), substituted=aom.concept("Virtuality")
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "member inheritance aspect"
            ),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "fulfill roles inheritance aspect"
            ),
        ),
    ],
)

esnm_aom.define_semantic_particle(
    "OperandInstance's inheritance from Operand",
    aom.particle("inheritance"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("OperandInstance (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_1"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Operand (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_2"),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("natural"), substituted=aom.concept("Virtuality")
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "member inheritance aspect"
            ),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "fulfill roles inheritance aspect"
            ),
        ),
    ],
)

esnm_aom.define_semantic_particle(
    "Operator’s inheritance from Instance",
    aom.particle("inheritance"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Operator (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_1"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Instance (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_2"),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("natural"), substituted=aom.concept("Virtuality")
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "member inheritance aspect"
            ),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "fulfill roles inheritance aspect"
            ),
        ),
    ],
)


esnm_aom.define_semantic_particle(
    "Operator’s inheritance from Instance",
    aom.particle("inheritance"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("OperandNode (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_1"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Substitutability (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_2"),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("natural"), substituted=aom.concept("Virtuality")
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "member inheritance aspect"
            ),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("disable"),
            substituted=aom.particle("inheritance").concept(
                "fulfill roles inheritance aspect"
            ),
        ),
    ],
)

esnm_aom.define_semantic_particle(
    "Conclusion’s inheritance from Net",
    aom.particle("inheritance"),
    [
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Conclusion (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_1"),
        ),
        ConcretizationSubstitution(
            substituting=esnm_aom.concept("Net (Assoc)"),
            substituted=(aom.concept("BaseNode"), "BaseNode_2"),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("natural"), substituted=aom.concept("Virtuality")
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "member inheritance aspect"
            ),
        ),
        ConcretizationSubstitution(
            substituting=aom.concept("nochange"),
            substituted=aom.particle("inheritance").concept(
                "fulfill roles inheritance aspect"
            ),
        ),
    ],
)

from eval_semantics.mappings.aom2eer import aom2eer

esnm_eer = [
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand"),
        pattern=aom2eer.patterns["bact_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator"),
        pattern=aom2eer.patterns["bact_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability"),
        pattern=aom2eer.patterns["bact_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept"),
        pattern=aom2eer.patterns["coll_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net (Coll)"),
        pattern=aom2eer.patterns["coll_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier"),
        pattern=aom2eer.patterns["coll_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Instance"),
        pattern=aom2eer.patterns["assoc_eer"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNode"),
        pattern=aom2eer.patterns["assoc_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandInstance"),
        pattern=aom2eer.patterns["assoc_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandOperator"),
        pattern=aom2eer.patterns["assoc_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNet"),
        pattern=aom2eer.patterns["assoc_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Modifier"),
        pattern=aom2eer.patterns["assoc_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net"),
        pattern=aom2eer.patterns["assoc_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Fact"),
        pattern=aom2eer.patterns["assoc_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Conclusion"),
        pattern=aom2eer.patterns["assoc_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Rule"),
        pattern=aom2eer.patterns["assoc_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's name"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's symbol"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's emotion"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's usage"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's type"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's core concept"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's type"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's variable"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's margin type"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's role"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's cardinality max"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's cardinality min"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's margin max"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's margin min"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator's type"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's depth"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's cfdf"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's int. quant. df"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's space quant. df"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's time quant. df"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's name"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's type"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's description"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier's cf"),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier's time quant."),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier's space quant."),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier's int. quant."),
        pattern=aom2eer.patterns["attr_eer"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Description of Operator's operator"),
        pattern=aom2eer.patterns["roledescr_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Description of Operands's operand"),
        pattern=aom2eer.patterns["roledescr_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle(
            "Description of Substitutability's substitutability"
        ),
        pattern=aom2eer.patterns["roledescr_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Description of Modifier"),
        pattern=aom2eer.patterns["assocdescr_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNode's inheritance from Operand"),
        pattern=aom2eer.patterns["inheritance_eer"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandInstance’s inheritance from Operand"),
        pattern=aom2eer.patterns["inheritance_eer"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandOperator's inheritance from Operand"),
        pattern=aom2eer.patterns["inheritance_eer"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNet's inheritance from Operand"),
        pattern=aom2eer.patterns["inheritance_eer"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandInstance's inheritance from Operand"),
        pattern=aom2eer.patterns["inheritance_eer"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator’s inheritance from Instance"),
        pattern=aom2eer.patterns["inheritance_eer"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator’s inheritance from Instance"),
        pattern=aom2eer.patterns["inheritance_eer"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Conclusion’s inheritance from Net"),
        pattern=aom2eer.patterns["inheritance_eer"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Instance's concept"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Instance's substitutability"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's modifier"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator's operand"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator's modifier"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandOperator's operator"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNet's net"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's net"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's conceptDef"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's subnet"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Fact's net"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Conclusion's fact"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Conclusion's rule"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Rule's net"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Modifier's modifier"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Modifier's concept"),
        pattern=aom2eer.patterns["role_eer"],
        alternative=1,
    ),
]

from eval_semantics.mappings.aom2orm import aom2orm

esnm_orm = [
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand"),
        pattern=aom2orm.patterns["bact_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator"),
        pattern=aom2orm.patterns["bact_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability"),
        pattern=aom2orm.patterns["bact_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept"),
        pattern=aom2orm.patterns["coll_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net (Coll)"),
        pattern=aom2orm.patterns["coll_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier"),
        pattern=aom2orm.patterns["coll_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Instance"),
        pattern=aom2orm.patterns["assoc_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNode"),
        pattern=aom2orm.patterns["assoc_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandInstance"),
        pattern=aom2orm.patterns["assoc_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandOperator"),
        pattern=aom2orm.patterns["assoc_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNet"),
        pattern=aom2orm.patterns["assoc_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Modifier"),
        pattern=aom2orm.patterns["assoc_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net"),
        pattern=aom2orm.patterns["assoc_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Fact"),
        pattern=aom2orm.patterns["assoc_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Conclusion"),
        pattern=aom2orm.patterns["assoc_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Rule"),
        pattern=aom2orm.patterns["assoc_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's name"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's symbol"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's emotion"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's usage"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's type"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's core concept"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's type"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's variable"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's margin type"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's role"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's cardinality max"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's cardinality min"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's margin max"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's margin min"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator's type"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's depth"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's cfdf"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's int. quant. df"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's space quant. df"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's time quant. df"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's name"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's type"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's description"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier's cf"),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier's time quant."),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier's space quant."),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier's int. quant."),
        pattern=aom2orm.patterns["attr_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Description of Operator's operator"),
        pattern=aom2orm.patterns["roledescr_orm"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Description of Operands's operand"),
        pattern=aom2orm.patterns["roledescr_orm"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle(
            "Description of Substitutability's substitutability"
        ),
        pattern=aom2orm.patterns["roledescr_orm"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Description of Modifier"),
        pattern=aom2orm.patterns["assocdescr_orm"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNode's inheritance from Operand"),
        pattern=aom2orm.patterns["inheritance_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandInstance’s inheritance from Operand"),
        pattern=aom2orm.patterns["inheritance_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandOperator's inheritance from Operand"),
        pattern=aom2orm.patterns["inheritance_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNet's inheritance from Operand"),
        pattern=aom2orm.patterns["inheritance_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandInstance's inheritance from Operand"),
        pattern=aom2orm.patterns["inheritance_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator’s inheritance from Instance"),
        pattern=aom2orm.patterns["inheritance_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator’s inheritance from Instance"),
        pattern=aom2orm.patterns["inheritance_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Conclusion’s inheritance from Net"),
        pattern=aom2orm.patterns["inheritance_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Instance's substitutability"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's modifier"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator's operand"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator's modifier"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandOperator's operator"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNet's net"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's net"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's conceptDef"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's subnet"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Fact's net"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Conclusion's fact"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Conclusion's rule"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Rule's net"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Modifier's modifier"),
        pattern=aom2orm.patterns["role_orm"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Modifier's concept"),
        pattern=aom2orm.patterns["role_orm"],
    ),
]

from eval_semantics.mappings.aom2uml import aom2uml

esnm_uml = [
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand"),
        pattern=aom2uml.patterns["bact_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator"),
        pattern=aom2uml.patterns["bact_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability"),
        pattern=aom2uml.patterns["bact_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept"),
        pattern=aom2uml.patterns["coll_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net (Coll)"),
        pattern=aom2uml.patterns["coll_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier"),
        pattern=aom2uml.patterns["coll_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Instance"),
        pattern=aom2uml.patterns["assoc_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNode"),
        pattern=aom2uml.patterns["assoc_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandInstance"),
        pattern=aom2uml.patterns["assoc_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandOperator"),
        pattern=aom2uml.patterns["assoc_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNet"),
        pattern=aom2uml.patterns["assoc_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Modifier"),
        pattern=aom2uml.patterns["assoc_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net"),
        pattern=aom2uml.patterns["assoc_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Fact"),
        pattern=aom2uml.patterns["assoc_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Conclusion"),
        pattern=aom2uml.patterns["assoc_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Rule"),
        pattern=aom2uml.patterns["assoc_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's name"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's symbol"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's emotion"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's usage"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's type"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Concept's core concept"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's type"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's variable"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's margin type"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's role"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's cardinality max"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's cardinality min"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's margin max"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's margin min"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator's type"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's depth"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's cfdf"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's int. quant. df"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's space quant. df"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Substitutability's time quant. df"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's name"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's type"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's description"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier's cf"),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier's time quant."),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier's space quant."),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Quantifier's int. quant."),
        pattern=aom2uml.patterns["attr_uml"],
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Description of Operator's operator"),
        pattern=aom2uml.patterns["roledescr_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Description of Operands's operand"),
        pattern=aom2uml.patterns["roledescr_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle(
            "Description of Substitutability's substitutability"
        ),
        pattern=aom2uml.patterns["roledescr_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Description of Modifier"),
        pattern=aom2uml.patterns["assocdescr_uml"],
        alternative=1,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNode's inheritance from Operand"),
        pattern=aom2uml.patterns["inheritance_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandInstance’s inheritance from Operand"),
        pattern=aom2uml.patterns["inheritance_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandOperator's inheritance from Operand"),
        pattern=aom2uml.patterns["inheritance_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNet's inheritance from Operand"),
        pattern=aom2uml.patterns["inheritance_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandInstance's inheritance from Operand"),
        pattern=aom2uml.patterns["inheritance_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator’s inheritance from Instance"),
        pattern=aom2uml.patterns["inheritance_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator’s inheritance from Instance"),
        pattern=aom2uml.patterns["inheritance_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Conclusion’s inheritance from Net"),
        pattern=aom2uml.patterns["inheritance_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Instance's substitutability"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operand's modifier"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator's operand"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Operator's modifier"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandOperator's operator"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("OperandNet's net"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's net"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's conceptDef"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Net's subnet"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Fact's net"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Conclusion's fact"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Conclusion's rule"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Rule's net"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Modifier's modifier"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
    SemanticParticleMapping(
        particle=esnm_aom.particle("Modifier's concept"),
        pattern=aom2uml.patterns["role_uml"],
        alternative=0,
    ),
]


esnm_aom.mappings = {}
esnm_aom.mappings["eer"] = esnm_eer
esnm_aom.mappings["orm"] = esnm_orm
esnm_aom.mappings["uml"] = esnm_uml
