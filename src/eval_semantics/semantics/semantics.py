from pydantic import BaseModel as Base
from typing import ForwardRef, Type, Optional, Union, Tuple, List
from enum import Enum
from eval_semantics import pyclom
import json

SemanticParticleRef = ForwardRef("SemanticParticle")
ConceptSubstitutionRef = ForwardRef("ConceptSubstitution")


class Concept(pyclom.Noun):
    model: Optional[str]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if "model" in kwargs.keys():
            self.type = kwargs["model"]

    def __eq__(self, other):
        if isinstance(other, SemanticAtom):
            return False

        if isinstance(other, tuple):  # labelend concepts are not equal to concepts
            return False

        name = self.name
        other_name = other.name

        if self.model != None:
            name += self.model

        if hasattr(other, "model") and other.model != None:
            other_name += other.model

        return name == other_name

    def __hash__(self):
        return hash(str(self))

    def __str__(self):
        if self.model != None:
            return self.model + ":" + self.name
        elif self.type != None:
            return self.type + ":" + self.name
        else:
            return self.name

    def __repr__(self):
        return str(self)

    @classmethod
    def serialize_name(cls, concept):
        if concept.type == None:
            return f"{concept.name}"
        else:
            return f"{concept.type}:{concept.name}"

    @classmethod
    def serialize(cls, concept):
        concept.general = [Concept.serialize_name(base) for base in concept.general]
        return json.loads(
            concept.json(
                exclude_unset=True, exclude={"type", "roles", "individual", "model"}
            )
        )


class SemanticAtom(Base):
    metamodel: str
    id: str
    concepts: List[Union[pyclom.Noun, Tuple[pyclom.Noun, str]]] = []
    verbalization: str
    is_possibilistic: bool

    class Config:
        json_encoders = {
            Concept: Concept.serialize_name,
            pyclom.Noun: Concept.serialize_name,
        }

    def is_monadic(self) -> bool:
        return len(self.concepts) == 1

    def is_polyadic(self) -> bool:
        return len(self.concepts) >= 1

    def __str__(self):
        return f"{self.metamodel}_{self.id}"

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        if not isinstance(other, SemanticAtom):
            return False
        return self.id == other.id and self.metamodel == other.metamodel

    def verbalize(self):
        def verb_repr(c):
            return f"[{c[1]}]" if isinstance(c, tuple) else f"[{c}]"

        concepts = [verb_repr(c) for c in self.concepts]
        return self.verbalization.format(*concepts)

    @classmethod
    def serialize_name(cls, atom):
        return str(atom)

    @classmethod
    def serialize(cls, atom):
        return json.loads(
            atom.json(exclude_unset=True, exclude={"metamodel"}, models_as_dict=False)
        )


class ConceptSubstitution(Base):
    substituting: Concept
    substituted: Union[Concept, Tuple[Concept, str], ConceptSubstitutionRef]
    context: Optional[List[SemanticAtom]]
    kind: str = "None"

    class Config:
        json_encoders = {
            Concept: Concept.serialize_name,
            SemanticAtom: SemanticAtom.serialize_name,
        }

    def substitutes(self) -> List[Concept]:
        origin = self
        subs = []
        while isinstance(origin, ConceptSubstitution):
            subs.append(origin.substituting)
            origin = origin.substituted
        subs.append(origin)
        return subs

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        if isinstance(other, ConceptSubstitution):
            substituting_eq = self.substituting.name == other.substituting.name
            substituted_eq = self.substituted.name == other.substituted.name
            return substituted_eq and substituting_eq

        if isinstance(other, Concept):
            return self.substituted == other

    @classmethod
    def serialize(cls, concept_subst):
        return json.loads(concept_subst.json(exclude_none=True, models_as_dict=False))


ConceptSubstitution.__config__.json_encoders[
    ConceptSubstitution
] = ConceptSubstitution.serialize


class CategorizationSubstitution(ConceptSubstitution):
    kind = "categorization"


class ConcretizationSubstitution(ConceptSubstitution):
    kind = "concretization"


class AtomOperation(Enum):
    Activation = "+"
    Deactivation = "-"

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return str(self.value)

    @classmethod
    def from_str(cls, symbol):
        if symbol == "+":
            return AtomOperation.Activation
        if symbol == "-":
            return AtomOperation.Deactivation


class ParticleAlternative(Base):
    alternatives: List["SemanticParticle"]

    @classmethod
    def serialize(cls, alt):
        alt.alternatives = [SemanticParticle.serialize(alt) for alt in alt.alternatives]
        return json.loads(alt.json(models_as_dict=False))


class SemanticParticle(Base):
    name: str
    model: str
    atoms: List[Union[SemanticAtom, tuple[SemanticAtom, AtomOperation]]] = []
    particles: List[Union[SemanticParticleRef, ParticleAlternative]] = []
    concepts: List[Union[Concept, ConceptSubstitution, Tuple[Concept, str]]]
    is_possibilistic: bool = False

    class Config:
        json_encoders = {
            SemanticAtom: SemanticAtom.serialize_name,
            Concept: Concept.serialize_name,
            ConceptSubstitution: ConceptSubstitution.serialize,
            ParticleAlternative: ParticleAlternative.serialize,
            pyclom.Noun: pyclom.Noun.serialize,
        }

    def __str__(self):
        return f"[{self.name}]_{self.model}"

    def __eq__(self, other):
        return self.model == other.model and self.name == other.name

    def __hash__(self):
        return hash(str(self))

    def concept(self, name: str):
        for concept in self.concepts:
            if isinstance(concept, ConceptSubstitution):
                if concept.substituting.name == name:
                    return concept
            elif isinstance(concept, Concept):
                if concept.name == name:
                    return concept
            elif isinstance(concept, tuple):
                if concept[1] == name:
                    return concept
        return None

    def with_atom_operation(self, id: str, operation: AtomOperation):

        new_particle = self.copy(deep=True)
        newatom = None
        for index, atom in enumerate(new_particle.atoms):

            if isinstance(atom, SemanticAtom) and atom.id == id:
                newatom = (atom, operation)
            elif isinstance(atom, tuple) and atom[0].id == id:
                newatom = (atom[0], operation)
                atom = (atom, operation)

            if newatom != None:
                new_particle.atoms[index] = newatom
                return new_particle

    def with_possibilistic(self):
        new_particle = self.copy(deep=True)
        new_particle.is_possibilistic = True
        return new_particle

    @classmethod
    def serialize(self, particle):
        return json.loads(particle.json(exclude_unset=True, models_as_dict=False))

    @classmethod
    def serialize_ref(self, particle):
        return json.loads(particle.json(exclude_unset=True, models_as_dict=False))


ParticleAlternative.update_forward_refs()
SemanticParticle.update_forward_refs()


SemanticParticle.__config__.json_encoders[SemanticParticle] = SemanticParticle.serialize
