from eval_semantics.mappings import (
    MappingPattern,
    Mapping,
    EmptyConcept,
    EmptySemanticAtom,
)
from eval_semantics.semantics.semantics import (
    Concept,
    SemanticAtom,
    ConceptSubstitution,
)
from eval_semantics.pyclom import CLOM_CONCEPT_TYPE

import logging
from pydantic import BaseModel as Base
from typing import Tuple, List, ForwardRef, Any
from itertools import product
from functools import reduce
import operator

EvaluationMetricRawRef = ForwardRef("EvaluationMetricRaw")
MetricsAggregationRef = ForwardRef("MetricsAggregation")
SemanticAtomsAggregationRef = ForwardRef("SemanticAtomsAggregation")

ROUND_PRECISION = 5


def weighted_average(elements, weights):
    elements = [e if e is not None else 0.0 for e in elements]
    return sum([el * w for el, w in zip(elements, weights)]) / sum(weights)


class EvaluationMetricRaw(Base):
    translated: List[Any] = []
    altered: List[Any] = []
    forced: List[Any] = []
    lost: List[Any] = []


class ConceptMetricRaw(EvaluationMetricRaw):
    translated: List[Tuple[Concept, Concept]] = []
    altered: List[Tuple[Concept, Concept]] = []
    forced: List[Tuple[Concept, Concept]] = []
    lost: List[Tuple[Concept, Concept]] = []


class SemanticAtomsMetricRaw(EvaluationMetricRaw):
    translated: List[SemanticAtom] = []
    altered: List[SemanticAtom] = []
    forced: List[SemanticAtom] = []
    lost: List[SemanticAtom] = []
    result: List[SemanticAtom] = []


class MetricsAggregation(Base):
    translated: float
    altered: float
    forced: float
    lost: float


class ConceptsAggregation(MetricsAggregation):
    def sum(self):
        return sum([self.translated, self.altered, self.forced, self.lost])

    def TC(self):
        return self.translated / self.sum()

    def AC(self):
        return self.altered / self.sum()

    def FC(self):
        return self.forced / self.sum()

    def LC(self):
        return self.lost / self.sum()

    @classmethod
    def aggregate(cls, raw_list: List[ConceptMetricRaw]) -> MetricsAggregationRef:
        return ConceptsAggregation(
            translated=reduce(operator.add, [len(m.translated) for m in raw_list]),
            altered=reduce(operator.add, [len(m.altered) for m in raw_list]),
            forced=reduce(operator.add, [len(m.forced) for m in raw_list]),
            lost=reduce(operator.add, [len(m.lost) for m in raw_list]),
        )

    def __add__(self, other):
        return ConceptsAggregation(
            translated=self.translated + other.translated,
            altered=self.altered + other.altered,
            forced=self.forced + other.forced,
            lost=self.lost + other.lost,
        )


class SemanticAtomsAggregation(MetricsAggregation):
    result: int

    def sum_src(self):
        return self.translated + self.altered + self.lost

    def sum_dst(self):
        return self.result + self.forced

    def TSA(self):
        try:
            return self.translated / self.sum_src()
        except ZeroDivisionError:
            return None

    def ASA(self):
        try:
            return self.altered / self.sum_src()
        except ZeroDivisionError:
            return None

    def LSA(self):
        try:
            return self.lost / self.sum_src()
        except ZeroDivisionError:
            return None

    def FSA(self):
        try:
            return self.forced / self.sum_dst()
        except ZeroDivisionError:
            return None

    @classmethod
    def aggregate(
        cls, raw_list: List[SemanticAtomsMetricRaw]
    ) -> SemanticAtomsAggregationRef:

        atom_to_tafl = {}

        def tafl_precedence(atom, symbol):
            curr = atom_to_tafl.get(atom)
            prec = [None, "t", "a", "l", "r", "f"]
            index1 = prec.index(curr)
            index2 = prec.index(symbol)
            return prec[max(index1, index2)]

        for raw in raw_list:  # raw_list is eval for particles rules
            for atom in raw.translated:
                atom_to_tafl[atom] = tafl_precedence(atom, "t")
            for atom in raw.altered:
                atom_to_tafl[atom] = tafl_precedence(atom, "a")
            for atom in raw.forced:
                atom_to_tafl[atom] = tafl_precedence(atom, "f")
            for atom in raw.lost:
                atom_to_tafl[atom] = tafl_precedence(atom, "l")
            for atom in raw.result:
                atom_to_tafl[atom] = tafl_precedence(atom, "r")

        vals = list(atom_to_tafl.values())
        agg = {x: vals.count(x) for x in vals}
        logging.debug(agg)
        return SemanticAtomsAggregation(
            translated=agg.get("t", 0),
            altered=agg.get("a", 0),
            forced=agg.get("f", 0),
            lost=agg.get("l", 0),
            result=agg.get("r", 0),
        )

    def __add__(self, other):
        return SemanticAtomsAggregation(
            translated=self.translated + other.translated,
            altered=self.altered + other.altered,
            forced=self.forced + other.forced,
            lost=self.lost + other.lost,
            result=self.result + other.result,
        )


class SemanticAtomsParticlesAggregation(MetricsAggregation):
    @classmethod
    def aggregate(cls, list: List[SemanticAtomsAggregation]):

        ssa = [agg.sum_src() for agg in list]
        rsa = [agg.sum_dst() for agg in list]

        tsaw, asaw, fsaw, lsaw = tuple(
            map(
                lambda num: round(num, ROUND_PRECISION),
                [
                    weighted_average([agg.TSA() for agg in list], ssa),
                    weighted_average([agg.ASA() for agg in list], ssa),
                    weighted_average([agg.FSA() for agg in list], rsa),
                    weighted_average([agg.LSA() for agg in list], ssa),
                ],
            )
        )

        assert (
            round(tsaw + asaw + lsaw, ROUND_PRECISION - 1) == 1
        ), "Metrics computation error"

        return SemanticAtomsParticlesAggregation(
            translated=tsaw,
            altered=asaw,
            forced=fsaw,
            lost=lsaw,
        )

    def TSA(self):
        return self.translated

    def ASA(self):
        return self.altered

    def LSA(self):
        return self.lost

    def FSA(self):
        return self.forced


class ConceptsParticlesAggregation(MetricsAggregation):
    @classmethod
    def aggregate(cls, list: List[ConceptsAggregation]):

        cp = [agg.sum() for agg in list]

        tc, ac, fc, lc = tuple(
            map(
                lambda num: round(num, ROUND_PRECISION),
                [
                    weighted_average([agg.TC() for agg in list], cp),
                    weighted_average([agg.AC() for agg in list], cp),
                    weighted_average([agg.LC() for agg in list], cp),
                    weighted_average([agg.FC() for agg in list], cp),
                ],
            )
        )

        return ConceptsParticlesAggregation(
            translated=tc, altered=ac, forced=fc, lost=lc
        )

    def TC(self):
        return self.translated

    def AC(self):
        return self.altered

    def FC(self):
        return self.forced

    def LC(self):
        return self.lost


class FinalMeasures(Base):
    rssa: float
    rsc: float
    rs: float

    @classmethod
    def compute(
        cls, sa: SemanticAtomsParticlesAggregation, c: ConceptsParticlesAggregation
    ):
        try:
            rssa = sa.TSA() / (sa.TSA() + sa.ASA() + sa.FSA() + sa.LSA())
        except:
            rssa = 0.0
        rsc = c.TC() / (c.TC() + c.AC() + c.FC() + c.LC())

        return FinalMeasures(
            rssa=round(rssa, ROUND_PRECISION),
            rsc=round(rsc, ROUND_PRECISION),
            rs=round((rssa + rsc) / 2, ROUND_PRECISION),
        )


listify = lambda x: x if isinstance(x, List) else [x]


def concept_pairs(mapping: Mapping):
    pairs = []

    input_list = listify(mapping.input)
    output_list = listify(mapping.output)

    for input in input_list:
        for output in output_list:
            if isinstance(input, Concept) or isinstance(input, ConceptSubstitution):
                if isinstance(output, Concept) or isinstance(
                    output, ConceptSubstitution
                ):
                    pairs.append((input, output))
    return pairs


def general_recursive(c):
    general = []

    if isinstance(c, tuple):
        c = c[0]

    if c.general:
        for concept in c.general:
            general.append(concept)

    for concept in general:
        if concept.type != CLOM_CONCEPT_TYPE:
            general.extend(general_recursive(concept))

    return general


def general_concepts(concept_list):
    """
    This function filters CLoM concepts in a leaf-preserving way
    """
    filtered = list(
        filter(lambda c: c.type == CLOM_CONCEPT_TYPE, general_recursive(concept_list))
    )

    # remove duplicates
    filtered = list(dict.fromkeys(filtered))

    logging.debug(filtered)
    return filtered


def mapping_evaluate_concept_pairs(
    pairs: List[Tuple[Concept, Concept]], mapping
) -> ConceptMetricRaw:

    metrics = ConceptMetricRaw()

    for pair in pairs:

        first, second = pair

        while isinstance(first, ConceptSubstitution):
            first = first.substituted

        if first.general == None or first.general == []:
            cc_first = [first]
        else:
            cc_first = general_concepts(first)

        if second.general == None or second.general == []:
            cc_second = [second]
        else:
            cc_second = general_concepts(second)

        common =  [value for value in cc_first if value in cc_second]

        cc_first = [value for value in cc_first if value not in common]
        cc_second = [value for value in cc_second if value not in common]

        prod = list(product(cc_first, cc_second))

        for element in prod:
            p1, p2 = element
            if (p2, p1) in prod and p1 != p2:
                prod.remove((p2, p1))

        for c in common:
            prod.append((c,c))

        logging.debug(prod)
       
        for p1, p2 in prod:

            if mapping.force_alternation:
                metrics.altered.append((p1, p2))
            elif mapping.force_translation:
                metrics.translated.append((p1, p2))
            elif isinstance(p1, EmptyConcept):
                metrics.forced.append((p1, p2))
            elif isinstance(p2, EmptyConcept):
                metrics.lost.append((p1, p2))
            elif p1 == p2 or p2.general != None and p1 in p2.general or (p1.general != None and  p2 in p1.general):
                metrics.translated.append((p1, p2))
            else:  # if p1 != p2:
                metrics.altered.append((p1, p2))

    return metrics


def mapping_evaluate_atoms(mapping: Mapping):  # need hints
    metrics = SemanticAtomsMetricRaw()

    input_list = listify(mapping.input)
    output_list = listify(mapping.output)
    atoms_src = list(filter(lambda a: isinstance(a, SemanticAtom), input_list))
    atoms_dest = list(filter(lambda a: isinstance(a, SemanticAtom), output_list))

    if len(atoms_src) > 0:

        if mapping.force_translation:
            metrics.translated += atoms_src
        elif len(atoms_dest) == 0:
            metrics.altered += atoms_src
        elif mapping.force_alternation or len(atoms_src) != len(atoms_dest):
            metrics.altered += atoms_src
        elif EmptySemanticAtom() in atoms_src:
            metrics.forced += atoms_dest
        elif EmptySemanticAtom() in atoms_dest:
            metrics.lost += atoms_src
        else:
            metrics.translated += atoms_src

    if len(atoms_dest) > 0:
        if EmptySemanticAtom() not in atoms_src:
            if EmptySemanticAtom() not in atoms_dest:
                metrics.result += atoms_dest
    return metrics


def pattern_evaluate(
    pattern: MappingPattern,
) -> Tuple[SemanticAtomsMetricRaw, ConceptMetricRaw]:
    sa_metrices = []
    c_metrices = []

    for mapping in pattern.rules:
        eval_atoms = mapping_evaluate_atoms(mapping)

        if eval_atoms:
            sa_metrices.append(eval_atoms)
        pairs = concept_pairs(mapping)
        if pairs:
            eval_concepts = mapping_evaluate_concept_pairs(pairs, mapping)
            if eval_concepts:
                c_metrices.append(eval_concepts)
    return (sa_metrices, c_metrices)
