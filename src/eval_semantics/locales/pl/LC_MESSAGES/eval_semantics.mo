��            )         �     �     �  	   �     �     �     �     �          !     0     B     W     q     �     �     �     �  "   �     �     �          -     A     W     i     w     �     �  B   �     �  �        �  
   �  	   �     �     �     �     �     �               2     J     j     �     �  "   �     �  .   �               4     J     h     �     �  %   �  #   �  #   �  M   	     l	                                                                                                                       
   	              (Deprecated) {text} Aborted! Arguments Commands Error: {message} Metrics type Missing argument Missing command. Missing option Missing parameter Missing {param_type} No such command {name!r}. No such option: {name} Options Show debug log Show this message and exit. Shows application version Try '{command} {option}' for help. Usage: Value must be an iterable. default: {default} mapping to generate metamodel to generate model to generate model to load output table format path to config file path to config file. path where result artifacts(figures + tables for thesis) are saved required Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
PO-Revision-Date: 2022-07-11 20:36+0200
Last-Translator: 
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
Generated-By: Babel 2.10.3
X-Generator: Poedit 3.0.1
 (Przestarzałe) {text} Przerwano! Argumenty Komendy Błąd: {message} typ miar Brakujący argument Brakująca komenda. Brakująca opcja Brakujący parametr Brakujący {param_type} Nie ma takiej komendy {name!r}. Nie ma takiej opcji: {name} Opcje Pokaż log debugowy Pokaż tę wiadomość i zakończ. Pokaż wersję aplikacji Wpisz '{command} {option}' aby uzyskać pomoc. Użycie: Wartość musi być iterowalna. domyślnie: {default} odwzorowanie do wygenerowania metamodel do wygenerowania model do wygenerowania model do załadowania wyjściowy format tabelaryczny danych ścieżka do pliku konfiguracyjnego ścieżka do pliku konfiguracyjnego ścieżka, pod którą artefakty wynikowe (obrazy i tabele) zostaną zapisane wymagane 