from eval_semantics.semantics.semantics import (
    CategorizationSubstitution,
    Concept,
    SemanticParticle,
    AtomOperation,
    ParticleAlternative,
    ConcretizationSubstitution,
)

from eval_semantics import pyclom
from eval_semantics.metamodels.uml import uml
from eval_semantics.metamodels.aom import aom

# Translational particle
from eval_semantics.mappings import (
    MappingPatternSet,
    Mapping,
    MappingPattern,
    EmptySemanticAtom,
    EmptyConcept,
)


coll_uml = MappingPattern(
    name="coll_uml",
    rules=[
        Mapping(
            symbol="R1",
            input=[aom.concept("Coll")],
            output=[uml.concept("Class")],
        ),
        Mapping(
            symbol="R1.1",
            input=[aom.semantic_atom("1")],
            output=[uml.semantic_atom("1")],
        ),
        Mapping(
            symbol="R1.2", input=[aom.semantic_atom("2")], output=EmptySemanticAtom()
        ),
    ],
    particle=SemanticParticle(
        name="coll",
        model="uml_aom",
        particles=[
            uml.particle("class").with_atom_operation("23", AtomOperation.Deactivation)
        ],
        concepts=[uml.concept("Class")],
    ),
)

attr_uml = MappingPattern(
    name="attr_uml",
    rules=[
        Mapping(
            symbol="R1",
            input=[aom.concept("Attr")],
            output=[uml.concept("Property")],
        ),
        Mapping(
            symbol="R1.1",
            input=[aom.semantic_atom("3")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.2",
            input=[aom.semantic_atom("4")],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.3",
            input=EmptySemanticAtom(),
            output=[uml.semantic_atom("3")],
        ),
        Mapping(
            symbol="R1.4",
            input=EmptySemanticAtom(),
            output=[uml.semantic_atom("4")],
        ),
        Mapping(
            symbol="R1.5",
            input=EmptySemanticAtom(),
            output=[uml.semantic_atom("5")],
        ),
        Mapping(
            symbol="R1.6",
            input=EmptySemanticAtom(),
            output=[uml.semantic_atom("6")],
        ),
        Mapping(
            symbol="R1.7",
            input=EmptySemanticAtom(),
            output=[uml.semantic_atom("7")],
        ),
        Mapping(
            symbol="R1.8",
            input=EmptySemanticAtom(),
            output=[uml.semantic_atom("8")],
        ),
        Mapping(
            symbol="R2",
            input=[aom.concept("Coll")],
            output=[uml.concept("Class")],
        ),
        Mapping(
            symbol="R2.1",
            input=[aom.semantic_atom("17")],
            output=[uml.semantic_atom("12")],
        ),
        Mapping(
            symbol="R3",
            input=[pyclom.math["positive integer"]],
            output=[pyclom.math["integer"], pyclom.math["unlimited integer"]],
        ),
        Mapping(
            symbol="R3.1",
            input=[aom.semantic_atom("18")],
            output=[uml.semantic_atom("17"), uml.semantic_atom("18")],
        ),
        Mapping(
            symbol="R4", input=[aom.concept("DataType")], output=[uml.concept("Type")]
        ),
        Mapping(
            symbol="R4.1",
            input=[aom.semantic_atom("19")],
            output=[uml.semantic_atom("19")],
        ),
        Mapping(
            symbol="R5",
            input=[aom.concept("Value")],
            output=[uml.concept("ValueSpecification")],
        ),
        Mapping(
            symbol="R5.1",
            input=[aom.semantic_atom("20")],
            output=[uml.semantic_atom("20")],
        ),
    ],
    particle=SemanticParticle(
        name="attr",
        model="uml_aom",
        particles=[uml.particle("attribute")],
        concepts=[
            uml.concept("Class"),
            uml.concept("Property"),
            uml.concept("Type"),
            uml.concept("ValueSpecification"),
            pyclom.math["unlimited integer"],
            pyclom.math["integer"],
        ],
    ),
)

assoc_uml = MappingPattern(
    name="assoc_uml",
    rules=[
        Mapping(
            symbol="R1",
            input=[aom.concept("Assoc")],
            output=[uml.concept("Association")],
        ),
        Mapping(
            symbol="R1.1",
            input=[aom.semantic_atom("1")],
            output=[uml.semantic_atom("1")],
        ),
        Mapping(
            symbol="R1.2", input=[aom.semantic_atom("2")], output=EmptySemanticAtom()
        ),
        Mapping(
            symbol="R2", input=[aom.concept("Assoc")], output=[uml.concept("Class")]
        ),
    ],
    particle=SemanticParticle(
        name="assoc",
        model="uml_aom",
        particles=[
            ParticleAlternative(
                alternatives=[
                    uml.particle("association"),
                    uml.particle("class"),
                ]
            )
        ],
        concepts=[uml.concept("Association"), uml.concept("Class")],
    ),
)

role_uml = MappingPattern(
    name="role_uml",
    rules=[
        Mapping(
            symbol="R1",
            input=[
                aom.concept("Role"),
            ],
            output=[uml.concept("Property")],
        ),
        Mapping(
            symbol="R1.1",
            input=[
                aom.semantic_atom("5"),
            ],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.2",
            input=[
                aom.semantic_atom("6"),
            ],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.3",
            input=[
                aom.semantic_atom("7"),
            ],
            output=[uml.semantic_atom("15")],
        ),
        Mapping(
            symbol="R1.4",
            input=[
                aom.semantic_atom("8"),
            ],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.5",
            input=[
                aom.semantic_atom("9"),
            ],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.6",
            input=[
                aom.semantic_atom("10"),
            ],
            output=EmptySemanticAtom(),
        ),
        Mapping(
            symbol="R1.7",
            input=[
                aom.semantic_atom("11"),
                aom.semantic_atom("12"),
            ],
            output=[uml.semantic_atom("3")],
        ),
        Mapping(
            symbol="R1.8", input=EmptySemanticAtom(), output=[uml.semantic_atom("2")]
        ),
        Mapping(
            symbol="R1.9", input=EmptySemanticAtom(), output=[uml.semantic_atom("4")]
        ),
        Mapping(
            symbol="R1.10", input=EmptySemanticAtom(), output=[uml.semantic_atom("5")]
        ),
        Mapping(
            symbol="R1.11", input=EmptySemanticAtom(), output=[uml.semantic_atom("8")]
        ),
        Mapping(
            symbol="R2",
            input=[aom.concept("Role"), aom.semantic_atom("21")],
            output=[
                uml.concept("Property"),
                uml.concept("Association"),
                uml.semantic_atom("14"),
            ],
        ),
        Mapping(
            symbol="R3",
            input=[aom.concept("Assoc")],
            output=[uml.concept("Association"), uml.concept("Class")],
        ),
        Mapping(
            symbol="R3.1",
            input=[aom.semantic_atom("21")],
            output=[uml.semantic_atom("13")],
        ),
        Mapping(
            symbol="R4",
            input=[aom.particle("role").concept("role end")],
            output=[uml.concept("Class")],
        ),
        Mapping(
            symbol="R4.1",
            input=[aom.semantic_atom("23")],
            output=[uml.semantic_atom("19")],
        ),
        Mapping(
            symbol="R4.2", input=[aom.concept("Assoc")], output=[uml.concept("Class")]
        ),
        Mapping(
            symbol="R5",
            input=[aom.particle("role").concept("mult supreme in owner")],
            output=EmptyConcept(),
        ),
        Mapping(
            symbol="R6",
            input=[aom.particle("role").concept("mult inf in owner")],
            output=EmptyConcept(),
        ),
        Mapping(
            symbol="R7",
            input=[aom.particle("role").concept("mult supreme in dest")],
            output=[pyclom.math["unlimited integer"]],
        ),
        Mapping(
            symbol="R7.1",
            input=[aom.semantic_atom("27")],
            output=[uml.semantic_atom("17")],
        ),
        Mapping(
            symbol="R8",
            input=[aom.particle("role").concept("mult inf in dest")],
            output=[pyclom.math["integer"]],
        ),
        Mapping(
            symbol="R8.1",
            input=[aom.semantic_atom("28")],
            output=[uml.semantic_atom("18")],
        ),
        Mapping(
            symbol="R9",
            input=[aom.semantic_atom("24")],
            output=[uml.semantic_atom("7")],
            force_alternation=True,
        ),
    ],
    particle=SemanticParticle(
        name="role",
        model="uml_aom",
        particles=[
            ParticleAlternative(
                alternatives=[
                    uml.particle("role"),
                    uml.particle("association"),
                ]
            )
        ],
        concepts=[
            uml.concept("Association"),
            uml.concept("Property"),
            CategorizationSubstitution(
                substituting=uml.concept("Class"), substituted=uml.concept("Type")
            ),
            pyclom.math["unlimited integer"],
            pyclom.math["integer"],
        ],
    ),
)

assocdescr_uml = MappingPattern(
    name="assocdescr_uml",
    rules=[
        Mapping(
            symbol="R1a",
            input=[aom.semantic_atom("22")],
            output=[uml.concept("AssociationClass")],
            force_translation = True            
        ),
        Mapping(
            symbol="R1",
            input=[aom.concept("Assoc"), aom.concept("Coll")],
            output=[uml.concept("AssociationClass")],
            #force_alternation = True           
        )
    ],
    particle=SemanticParticle(
        name="assocdescr",
        model="uml_aom",
        particles=[uml.particle("association class")],
        concepts=[uml.concept("AssociationClass")],
    ),
)

roledescr_uml = MappingPattern(
    name="roledescr_uml",
    rules=[
        Mapping(
            symbol="IR1",
            input=[aom.semantic_atom("29")],
            output=[uml.semantic_atom("14")],
            force_alternation=True,
        ),
        Mapping(symbol="IR2", input=[aom.concept("Role")], output=EmptyConcept()),
        Mapping(symbol="IR3", input=EmptyConcept(), output=[uml.concept("Property")]),
        Mapping(
            symbol="IR3", input=EmptyConcept(), output=[uml.concept("Association")]
        ),
        Mapping(
            symbol="IR4", input=[aom.concept("Coll")], output=[uml.concept("Class")]
        ),
        Mapping(
            symbol="IR5",
            input=EmptyConcept(),
            output=[
                pyclom.math["number"](1),
                pyclom.math["number"](0),
            ],
        ),
    ],
    particle=SemanticParticle(
        name="roledescr",
        model="uml_aom",
        particles=[uml.particle("role")],
        concepts=[
            uml.concept("Class"),
            uml.concept("Property"),
            uml.concept("Association"),
            ConcretizationSubstitution(
                substituting=pyclom.math["number"](1),
                substituted=pyclom.math["unlimited integer"],
            ),
            ConcretizationSubstitution(
                substituting=pyclom.math["number"](0),
                substituted=pyclom.math["integer"],
            ),
        ],
    ),
)


inheritance_uml = MappingPattern(
    name="inheritance_uml",
    rules=[
        Mapping(
            symbol="R0",
            input=[aom.concept("Inheritance")],
            output=[uml.concept("Generalization")],
        ),
        Mapping(
            symbol="R0.1", input=[aom.concept("Coll")], output=[uml.concept("Class")]
        ),
        Mapping(
            symbol="R1",
            input=[aom.semantic_atom("13")],
            output=[
                uml.semantic_atom("21"),
                uml.semantic_atom("22"),
            ],
        ),
        Mapping(
            symbol="R1.1",
            input=[aom.concept("Inheritability"), aom.semantic_atom("14")],
            output=[EmptyConcept(), EmptySemanticAtom()],
        ),
        Mapping(
            symbol="R1.2",
            input=[aom.concept("Inheritability"), aom.semantic_atom("15")],
            output=[EmptyConcept(), EmptySemanticAtom()],
        ),
        Mapping(
            symbol="R1.3",
            input=[aom.concept("Virtuality"), aom.semantic_atom("16")],
            output=[EmptyConcept(), EmptySemanticAtom()],
        ),
        Mapping(symbol="R2", input=[aom.concept("Inheritance")], output=EmptyConcept()),
        Mapping(
            symbol="R2.1-2",
            input=[aom.concept("Assoc")],
            output=[uml.concept("Association")],
        ),
        Mapping(
            symbol="R2.3",
            input=[aom.semantic_atom("21")],
            output=[uml.semantic_atom("13")],
        ),
    ],
    particle=SemanticParticle(
        name="inheritance",
        model="uml_aom",
        particles=[
            ParticleAlternative(
                alternatives=[
                    uml.particle("generalization"),
                    uml.particle("association"),
                ]
            )
        ],
        concepts=[
            uml.concept("Generalization"),
            uml.particle("generalization").concept("generalization"),
            uml.particle("generalization").concept("specialization"),
            (uml.concept("Association"), "Association_1"),
            (uml.concept("Association"), "Association_2"),
        ],
    ),
)

bact_uml = MappingPattern(
    name="bact_uml",
    rules=[
        Mapping(
            symbol="R1",
            input=[
                aom.particle("bact").concept("data aspect"),
                aom.particle("bact").concept("relationship aspect"),
                aom.particle("bact").concept("unbreakable connection"),
                pyclom.math["number"](1),
            ],
            output=[uml.concept("Class")],
        ),
        Mapping(
            symbol="R1.1",
            input=[aom.semantic_atom("1")],
            output=[uml.semantic_atom("1")],
        ),
        Mapping(
            symbol="R1.2",
            input=[aom.concept("Role")],
            output=[uml.concept("Association")],
        ),
        Mapping(
            symbol="R1.3",
            input=[aom.semantic_atom("11")],
            output=[uml.concept("Class")],
        ),
        Mapping(
            symbol="R1.4",
            input=[aom.semantic_atom("12")],
            output=[uml.concept("Class")],
        ),
        Mapping(
            symbol="R1.5",
            input=[pyclom.math["number"](1), aom.semantic_atom("25")],
            output=[uml.concept("Class")],
        ),
        Mapping(
            symbol="R1.6",
            input=[pyclom.math["number"](1), aom.semantic_atom("26")],
            output=[uml.concept("Class")],
        ),
        Mapping(
            symbol="R1.7",
            input=[pyclom.math["number"](1), aom.semantic_atom("27")],
            output=[uml.concept("Class")],
        ),
        Mapping(
            symbol="R1.8",
            input=[pyclom.math["number"](1), aom.semantic_atom("28")],
            output=[uml.concept("Class")],
        ),
    ],
    particle=SemanticParticle(
        name="bact",
        model="uml_aom",
        particles=[
            uml.particle("class").with_atom_operation("1", AtomOperation.Deactivation)
        ],
        concepts=[uml.concept("Class")],
    ),
)


aom2uml = MappingPatternSet(
    source="aom",
    target="uml",
    patterns={
        coll_uml.name: coll_uml,
        attr_uml.name: attr_uml,
        assoc_uml.name: assoc_uml,
        role_uml.name: role_uml,
        assocdescr_uml.name: assocdescr_uml,
        roledescr_uml.name: roledescr_uml,
        inheritance_uml.name: inheritance_uml,
        bact_uml.name: bact_uml,
    },
)
