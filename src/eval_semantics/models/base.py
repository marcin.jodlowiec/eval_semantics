import logging
from pydantic import BaseModel as Base, validator
from typing import List, Tuple, Optional, Dict
from pathlib import Path
import json

from eval_semantics import metamodels
from eval_semantics import pyclom
from eval_semantics import mappings

from eval_semantics.semantics.semantics import (
    Concept,
    SemanticAtom,
    SemanticParticle,
    ConceptSubstitution,
    ConcretizationSubstitution,
    CategorizationSubstitution,
    AtomOperation,
)

from eval_semantics.models.particle import ModelSemanticParticle
from eval_semantics.mappings import SemanticParticleMapping


class ModelConcept(Concept):
    @classmethod
    def serialize(cls, concept):
        concept.general = [f"{base.model}:{base.name}" for base in concept.general]
        return json.loads(
            concept.json(exclude_unset=True, exclude={"individual", "model"})
        )


class ModelSemantics(Base):
    metamodel: str
    name: str
    concepts: List[ModelConcept] = []
    particles: List[ModelSemanticParticle] = []
    mappings: Optional[Dict[str, List[SemanticParticleMapping]]]

    class Config:
        validate_assignment = (True,)
        json_encoders = {
            ModelConcept: ModelConcept.serialize,
            ModelSemanticParticle: ModelSemanticParticle.serialize,
            SemanticParticleMapping: SemanticParticleMapping.serialize,
        }

    @validator("metamodel")
    def metamodel_from_repository(cls, value):
        assert metamodels.base.repository.is_registered(
            value
        ), "metamodel not registered"
        return value

    def define_concept(self, name: str, base: Concept):
        c = ModelConcept(
            model=self.modelname(), name=name, individual=True, general=[base]
        )
        self.concepts.append(c)
        return c

    def concept(self, name):
        for concept in self.concepts:
            if concept.name == name:
                return concept
        raise Exception(f"Concept {name} not found in {self.name}")

    def define_semantic_particle(
        self,
        name: str,
        base: SemanticParticle,
        concretizations: List[ConcretizationSubstitution],
        operations: List[Tuple[SemanticAtom, AtomOperation]] = [],
    ):

        model_particle = ModelSemanticParticle(
            base=base,
            name=name,
            model=self.modelname(),
            atoms=base.atoms,
            concepts=concretizations,
        )

        # check activation
        for k, atom in enumerate(model_particle.atoms):
            if isinstance(atom, SemanticAtom) and atom.is_possibilistic:
                # we have possibilistic atom, need to activate or deactivate
                found = False
                for op in operations:
                    if isinstance(op, tuple) and op[0] == atom:
                        model_particle.atoms[k] = op
                        found = True
                        break
                if not found:
                    raise Exception(
                        f"Possibiistic atom {atom} not neither activated nor deactivated"
                    )

        self.particles.append(model_particle)

    def particle(self, name):
        for particle in self.particles:
            if particle.name == name:
                return particle
        raise Exception(f"Particle {name} not found in {self.name}")

    def modelname(self):
        return f"{self.name}-{self.metamodel}"


class ModelRepository(object):
    def __init__(self):
        self.models: dict[str, ModelSemantics] = {}

    def register(self, name, model: ModelSemantics):
        logging.debug(f"Registering model {name}...")
        self.models[name] = model

    def model(self, name):
        try:
            return self.models[name]
        except KeyError:
            raise Exception(f'Model "{name}" not registered')

    def is_registered(self, name) -> bool:
        return name in self.models.keys()

    def load_from_json(self, path: Path):
        with open(path) as file:
            data = json.loads(file.read())
            return self.load_from_dict(data)

    def load_from_dict(self, data):
        model_name = data["name"]
        metamodel_name = data["metamodel"]
        metamodel = metamodels.base.repository.metamodel(metamodel_name)
        model = ModelSemantics(name=model_name, metamodel=metamodel_name)

        def find_model_concept(concept):
            concept_name_split = concept.split(":")
            if len(concept_name_split) >= 2:
                type, name = concept_name_split
                if type == model.modelname():  # Model concept
                    return model.concept(name)
                elif type == metamodel_name:  # Metamodel concept
                    return metamodel.concept(name)
                elif type == pyclom.CLOM_CONCEPT_TYPE:
                    return pyclom.clom[name]
                elif type == pyclom.MATH_CONCEPT_TYPE:
                    try:
                        return pyclom.math[name]
                    except KeyError:  # No defined concept, generate instead
                        return pyclom.math["number"](name)
                else:
                    raise Exception(f"Could not recognize concept {type}:{name}")
            else:
                return Concept(name=concept, individual=True)

        def find_concept(concept):
            def find_concept_inner(concept_name):
                concept_name_split = concept_name.split(":")
                if len(concept_name_split) == 2:
                    type, name = concept_name_split
                    if type == metamodel_name:  # metamodel_concept
                        return metamodel.concept(name)
                    elif type == pyclom.CLOM_CONCEPT_TYPE:
                        return pyclom.clom[name]
                    elif type == pyclom.MATH_CONCEPT_TYPE:
                        try:
                            return pyclom.math[name]
                        except KeyError:  # No defined concept, generate instead
                            return pyclom.math["number"](name)
                else:
                    return Concept(name=concept_name, individual=True)

            if isinstance(concept, str):  # concept
                return find_concept_inner(concept)
            elif isinstance(concept, list):  # labeled
                return (find_concept_inner(concept[0]), concept[1])
            elif isinstance(concept, dict):  # substitution
                if concept["kind"] == "categorization":
                    return CategorizationSubstitution(
                        substituting=find_concept(concept["substituting"]),
                        substituted=find_concept(concept["substituted"]),
                    )
                if concept["kind"] == "concretization":
                    return ConcretizationSubstitution(
                        substituting=find_concept(concept["substituting"]),
                        substituted=find_concept(concept["substituted"]),
                    )

            raise Exception(f'Could not find concept: "{concept}"')

        def find_atom(id):
            return metamodel.semantic_atom(id)

        # Load model concepts
        for concept in data["concepts"]:
            base = find_concept(concept["general"][0])
            model.define_concept(name=concept["name"], base=base)

        for particle in data["particles"]:
            particle_name = particle["name"]
            particle_base = particle["base"]

            base = metamodel.particle(particle_base)

            particle_atoms = []
            for atom in particle["atoms"]:
                if isinstance(atom, str):  # no operation
                    mm, id = atom.split("_")
                    particle_atoms.append(find_atom(id))
                elif isinstance(atom, list):  # operation
                    mm, id = atom[0].split("_")
                    particle_atoms.append(
                        (find_atom(id), AtomOperation.from_str(atom[1]))
                    )

            concretizations = []

            for concept in particle["concepts"]:
                substituting = find_model_concept(concept["substituting"])
                substituted = find_concept(concept["substituted"])

                csubst = None
                match concept["kind"]:
                    case "concretization":
                        csubst = ConcretizationSubstitution(
                            substituting=substituting, substituted=substituted
                        )
                    case "categorization":
                        csubst = CategorizationSubstitution(
                            substituting=substituting, substituted=substituted
                        )

                concretizations.append(csubst)

            model.define_semantic_particle(
                name=particle_name,
                base=base,
                concretizations=concretizations,
                operations=particle_atoms,
            )

        model_mappings = {}
        mapping_repo = mappings.repository

        data_mappings = data.get("mappings")

        if data_mappings:
            for dest_name, mapping in data_mappings.items():
                dest_mm = mapping_repo.pattern_set(metamodel_name, dest_name)
                model_mappings[dest_name] = []
                for rule in mapping:
                    particle = model.particle(rule["particle"])
                    pattern = dest_mm.patterns[rule["pattern"]]
                    alternative = rule.get("alternative")
                    model_mappings[dest_name].append(
                        SemanticParticleMapping(
                            particle=particle, pattern=pattern, alternative=alternative
                        )
                    )

        model.mappings = model_mappings
        self.register(model_name, model)

        return model


repository = ModelRepository()
