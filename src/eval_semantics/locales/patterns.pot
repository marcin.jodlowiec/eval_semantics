# Translations template for PROJECT.
# Copyright (C) 2022 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2022.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2022-07-10 20:01+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: src/eval_semantics/plotting/__init__.py:26
msgid "coll"
msgstr ""

#: src/eval_semantics/plotting/__init__.py:27
msgid "attr"
msgstr ""

#: src/eval_semantics/plotting/__init__.py:28
msgid "assoc"
msgstr ""

#: src/eval_semantics/plotting/__init__.py:29
msgid "role"
msgstr ""

#: src/eval_semantics/plotting/__init__.py:30
msgid "assocdescr"
msgstr ""

#: src/eval_semantics/plotting/__init__.py:31
msgid "roledescr"
msgstr ""

#: src/eval_semantics/plotting/__init__.py:32
msgid "inheritance"
msgstr ""

#: src/eval_semantics/plotting/__init__.py:33
msgid "bact"
msgstr ""

msgid "aggregated measures"
msgstr ""

msgid "Results for aggregated measures"
msgstr ""

msgid "resulting measures"
msgstr ""

msgid "Results for resulting measures"
msgstr ""

msgid "Evaluation results for aggregated measures"
msgstr ""

msgid "Evaluation results for resulting measures"
msgstr ""
