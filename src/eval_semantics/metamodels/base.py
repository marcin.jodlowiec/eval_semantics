from eval_semantics import pyclom
from eval_semantics.pyclom import Noun
from eval_semantics.semantics.semantics import (
    AtomOperation,
    Concept,
    SemanticAtom,
    SemanticParticle,
    ConceptSubstitution,
    CategorizationSubstitution,
    ConcretizationSubstitution,
)

import logging
from pydantic import BaseModel as Base, root_validator
from typing import Dict, List
from pathlib import Path
import json


class MetamodelSemantics(Base):
    name: str
    concepts: List[Concept] = []
    atoms: List[SemanticAtom] = []
    particles: List[SemanticParticle] = []

    class Config:
        validate_assignment = True
        json_encoders = {
            Concept: Concept.serialize,
            SemanticAtom: SemanticAtom.serialize,
            SemanticParticle: SemanticParticle.serialize,
        }

    def define_concept(self, concept_name: str, general: list[Noun] = []):

        concept = Concept(
            model=self.name, name=concept_name, individual=True, general=general
        )

        self.concepts.append(concept)

    def concept(self, name):
        for concept in self.concepts:
            if concept.name == name:
                return concept
        raise Exception(f"Concept {name} not found in {self.name}")

    def define_semantic_atom(self, id, is_possibilistic, verbalization, concepts):
        atom = SemanticAtom(
            metamodel=self.name,
            id=id,
            is_possibilistic=is_possibilistic,
            verbalization=verbalization,
            concepts=concepts,
        )
        self.atoms.append(atom)

    def semantic_atom(self, id):
        for atom in self.atoms:
            if atom.id == id:
                return atom
        return None

    def define_semantic_particle(self, **kwargs):
        kwargs["model"] = self.name
        particle = SemanticParticle(**kwargs)
        self.particles.append(particle)

    def particle(self, name):
        for particle in self.particles:
            if particle.name == name:
                return particle.copy(deep=True)
        raise Exception(f"Particle {name} not found in {self.name}")


class MetamodelRepository(object):
    def __init__(self):
        self.metamodels: Dict[str, MetamodelSemantics] = {}

    def register(self, name, metamodel: MetamodelSemantics):
        logging.debug(f"Registering metamodel {name}...")
        self.metamodels[name] = metamodel

    def metamodel(self, name):
        try:
            return self.metamodels[name]
        except KeyError:
            raise Exception(f'Metamodel "{name}" not registered')

    def is_registered(self, name) -> bool:
        return name in self.metamodels.keys()

    def load_from_json(self, path: Path):
        with open(path) as file:
            data = json.loads(file.read())
            self.load_from_dict(data)

    def load_from_dict(self, data):
        metamodel_name = data["name"]
        metamodel = MetamodelSemantics(name=metamodel_name)

        def find_concept(concept):
            def find_concept_inner(concept_name):
                concept_name_split = concept_name.split(":")
                if len(concept_name_split) == 2:
                    type, name = concept_name_split
                    if type == metamodel_name:  # metamodel_concept
                        return metamodel.concept(name)
                    elif type == pyclom.CLOM_CONCEPT_TYPE:
                        return pyclom.clom[name]
                    elif type == pyclom.MATH_CONCEPT_TYPE:
                        try:
                            return pyclom.math[name]
                        except KeyError:  # No defined concept, generate instead
                            return pyclom.math["number"](name)

                else:
                    return Concept(name=concept_name, individual=True)

            if isinstance(concept, str):  # concept
                return find_concept_inner(concept)
            elif isinstance(concept, list):  # labeled
                return (find_concept_inner(concept[0]), concept[1])
            elif isinstance(concept, dict):  # substitution

                context = concept.get("context")
                if context:
                    context = [find_atom(atom.split("_")[1]) for atom in context]

                if concept["kind"] == "categorization":
                    return CategorizationSubstitution(
                        substituting=find_concept(concept["substituting"]),
                        substituted=find_concept(concept["substituted"]),
                        context=context,
                    )
                if concept["kind"] == "concretization":
                    return ConcretizationSubstitution(
                        substituting=find_concept(concept["substituting"]),
                        substituted=find_concept(concept["substituted"]),
                        context=context,
                    )

            raise Exception(f'Could not find concept: "{concept}"')

        def find_atom(id):
            return metamodel.semantic_atom(id)

        # Load concepts
        for concept in data["concepts"]:
            general_concepts = []
            for general in concept["general"]:
                general_concepts.append(find_concept(general))
            metamodel.define_concept(
                concept_name=concept["name"], general=general_concepts
            )

        # Load semantic Atoms
        for atom in data["atoms"]:
            atom_concepts = []
            for concept in atom["concepts"]:
                atom_concepts.append(find_concept(concept))

            metamodel.define_semantic_atom(
                id=atom["id"],
                is_possibilistic=atom["is_possibilistic"],
                verbalization=atom["verbalization"],
                concepts=atom_concepts,
            )

        # Load particles
        for particle in data["particles"]:
            particle_name = particle["name"]

            particle_atoms = []
            for atom in particle["atoms"]:
                if isinstance(atom, str):  # no operation
                    mm, id = atom.split("_")
                    particle_atoms.append(find_atom(id))
                elif isinstance(atom, list):  # operation
                    mm, id = atom[0].split("_")
                    particle_atoms.append(
                        (find_atom(id), AtomOperation.from_str(atom[1]))
                    )

            particle_concepts = []
            for concept in particle["concepts"]:
                particle_concepts.append(find_concept(concept))

            metamodel.define_semantic_particle(
                name=particle_name, atoms=particle_atoms, concepts=particle_concepts
            )

        self.register(metamodel_name, metamodel)


repository = MetamodelRepository()
