# Development mode check
import os
import sys
from pathlib import Path

if sys.platform.startswith('win'):
    import locale
    if os.getenv('LANG') is None:
        lang, enc = locale.getdefaultlocale()
        os.environ['LANG'] = lang

if getattr(sys, "frozen", False):
    application_path = Path(sys.executable).parent
    os.environ["LOCALE_PATH"] = str(application_path / "locales")
elif "DEVELOPMENT" in os.environ:
    sys.path.append(r"src/")
    os.environ["LOCALE_PATH"] = "src/eval_semantics/locales"
else:
    import pkg_resources

    locale_path = pkg_resources.resource_filename("eval_semantics", "locales")
    os.environ["LOCALE_PATH"] = locale_path

from eval_semantics import commands
import typer, click
import gettext

# i18n
try:
    eval_semantics_translation = gettext.translation(
        "eval_semantics", localedir=os.environ["LOCALE_PATH"]
    )
    eval_semantics_translation.install()
    _ = eval_semantics_translation.gettext
except:
    _ = gettext.gettext
finally:
    click.core._ = _
    click.formatting._ = _
    click.exceptions._ = _
    typer.core._ = _


app = typer.Typer(add_completion=False)

commands.make_commands(app)


if __name__ == "__main__":
    app()
